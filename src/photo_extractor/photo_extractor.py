from __future__ import annotations

from typing import Optional, TYPE_CHECKING

import numpy as np
from twisted.logger import Logger

from src.settings import Settings
from src.system_operations.scanner_operator import SCANNED_FILENAME, SCANNED_FORMAT
from src.utils import cv2_tools
from time import time

if TYPE_CHECKING:
    from src.config import Config

settings = Settings()
PHOTO_FILENAME_PREFIX = "photo_"
OUTPUT_FORMAT = "png"


class PhotoExtractError(Exception):
    pass


NO_PHOTOS_FOUND = "No images found in the scanned document."


class PhotoExtractor:
    log = Logger()

    def __init__(
        self,
        config: Config,
    ):
        self.background_threshold = config.background_threshold
        self.min_aspect_ratio = config.min_aspect_ratio
        self.max_aspect_ratio = config.max_aspect_ratio
        self.min_photo_area = config.min_photo_area
        self.erode_iterations = config.erode_iterations

    def is_valid_photo(self, w: int, h: int) -> bool:
        return bool(
            w * h > self.min_photo_area
            and self.min_aspect_ratio <= w / h <= self.max_aspect_ratio
        )

    def remove_borders(self, img: np.ndarray) -> np.ndarray:
        contours = cv2_tools.get_contours(
            img=img,
            erode_iterations=self.erode_iterations,
            min_thresh=self.background_threshold,
        )
        largest_contour = cv2_tools.get_largest_contour(contours)
        return self.crop_image(largest_contour, img)

    def crop_image(self, cntr: np.ndarray, img: np.ndarray) -> Optional[np.ndarray]:
        x, y, w, h = cv2_tools.get_bounding_boxes(cntr)
        if not self.is_valid_photo(w=w, h=h):
            return
        return img[y : y + h, x : x + w]

    def crop_and_remove_borders(self, cntr: np.ndarray, img: np.ndarray) -> np.ndarray:
        cropped = self.crop_image(cntr=cntr, img=img)
        if cropped is not None:
            return self.remove_borders(cropped)

    def deskew_and_crop(
        self, img: np.ndarray, cntr: np.ndarray
    ) -> Optional[np.ndarray]:
        img = cv2_tools.deskew(img=img, cntr=cntr)
        return self.crop_and_remove_borders(cntr, img)

    def extract_photos(
        self,
    ) -> None:
        img = cv2_tools.create_image_array(
            f"{settings.sys_directories.scanned_images_path}{SCANNED_FILENAME}.{SCANNED_FORMAT}"
        )
        contours = cv2_tools.get_contours(
            img=img,
            erode_iterations=self.erode_iterations,
            min_thresh=self.background_threshold,
        )
        valid_photos = []
        for cntr in reversed(contours):
            result = self.deskew_and_crop(img=img, cntr=cntr)
            if result is None:
                continue
            filename = f"{PHOTO_FILENAME_PREFIX}{time()}.{OUTPUT_FORMAT}"

            cv2_tools.write_image_file(
                img=result,
                path=f"{settings.sys_directories.scanned_images_path}{filename}",
            )
            valid_photos.append(filename)

        if not valid_photos:
            self.log.info(NO_PHOTOS_FOUND)
            raise PhotoExtractError(NO_PHOTOS_FOUND)
