from unittest import mock
from test.samples import data_samples


def patch_time(locations: list[str]):
    time = mock.MagicMock(
        side_effect=[data_samples.TIMESTAMP_1, data_samples.TIMESTAMP_2]
    )
    for location in locations:
        patcher = mock.patch(location + ".time", time)
        patcher.start()
