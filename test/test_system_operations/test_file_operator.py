from unittest import mock

from twisted.trial import unittest

from src.system_operations.file_operator import FileOperator
from test.patchers.settings import patch_settings
from test.samples import data_samples

MODULE_PATH = "src.system_operations.file_operator"


class TestFileOperator(unittest.TestCase):
    def setUp(self):
        patch_settings([MODULE_PATH])
        self.patch_os()
        self.patch_shutil()
        self.patch_path()
        self.file_operator = FileOperator()

    def patch_os(self):
        self.os = mock.MagicMock()
        self.os.listdir.return_value = data_samples.LISTDIR_RESPONSE_TEMP
        self.os.path.expanduser.return_value = data_samples.USER_DIR
        patcher = mock.patch(MODULE_PATH + ".os", self.os)
        patcher.start()

    def patch_shutil(self):
        self.shutil = mock.MagicMock()
        patcher = mock.patch(MODULE_PATH + ".shutil", self.shutil)
        patcher.start()

    def patch_path(self):
        self.path = mock.MagicMock()
        patcher = mock.patch(MODULE_PATH + ".Path", self.path)
        patcher.start()

    def test_save_file(self):
        self.file_operator.save_file(
            source_filename=data_samples.TEMP_FILENAME1,
            dest_filename=data_samples.OUTPUT_FILENAME1,
        )
        self.shutil.copy.assert_called_once_with(
            f"{data_samples.SCANNED_IMAGES_PATH}{data_samples.TEMP_FILENAME1}",
            f"{data_samples.OUTPUT_DIR_LINUX}{data_samples.OUTPUT_FILENAME1}",
        )

    def test_delete_file(self):
        self.file_operator.delete_file(data_samples.TEMP_FILENAME1)
        self.os.remove.assert_called_once_with(
            f"{data_samples.SCANNED_IMAGES_PATH}{data_samples.TEMP_FILENAME1}"
        )

    def test_list_files(self):
        result = self.file_operator.list_files(data_samples.SCANNED_IMAGES_PATH)
        self.assertEqual(result, data_samples.LISTDIR_RESPONSE_TEMP)

    def test_remove_dir(self):
        self.file_operator.remove_dir(data_samples.SCANNED_IMAGES_PATH)
        self.shutil.rmtree.assert_called_once_with(data_samples.SCANNED_IMAGES_PATH)

    def test_create_directory(self):
        self.file_operator.create_directory(data_samples.SCANNED_IMAGES_PATH)
        self.path.assert_called_once_with(data_samples.SCANNED_IMAGES_PATH)
        self.path.return_value.mkdir.assert_called_once()

    def test_file_exists(self):
        self.file_operator.file_exists(data_samples.SCANNED_TEMP_FILE_PATH)
        self.os.path.exists.assert_called_once_with(data_samples.SCANNED_TEMP_FILE_PATH)
