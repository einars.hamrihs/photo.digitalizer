from typing import Optional

from src.models import general, request_models

PATH_MODEL_MAP = {
    general.AppPath.main.path: request_models.MainRequest,
    general.ExtractConfigPath.update.path: request_models.ScanRequest,
    general.AppPath.scan.path: request_models.ScanRequest,
    general.AppPath.save.path: request_models.SaveRequest,
    general.AppPath.delete.path: request_models.MainPostRequest,
    general.AppPath.accordion.path: request_models.UpdateAccordionRequest,
    general.SaveConfigPath.update.path: request_models.SaveRequest,
}


def create_args_model(path: str, params_dict: Optional[dict] = None):
    return PATH_MODEL_MAP.get(path).create(params_dict)
