import os
import platform

from pydantic import field_validator
from pydantic_settings import BaseSettings
from pydantic_settings import SettingsConfigDict

from src.models import general
from src.utils.path_utils import get_path

USER_HOMEDIR = os.path.expanduser("~")
APPDATA = os.environ.get("LocalAppData")
LINUX_DATA_DIR = ".Pixelyse"
WINDOWS_DATA_DIR = "Pixelyse"


def user_dir():
    snap_real_home = os.environ.get("SNAP_REAL_HOME")
    return snap_real_home if snap_real_home else USER_HOMEDIR


class SystemDirectories(BaseSettings):
    scanned_images_path: str
    config_dir: str
    output_dir: str
    scripts_dir: str
    temp_dir: str

    abs_scripts_dir = field_validator("scripts_dir")(get_path)


class Settings(BaseSettings):
    model_config = SettingsConfigDict(env_file=".env", env_file_encoding="utf-8")

    # default values to None for unit tests
    app_name: str = "Pixelyse"
    templates_path: str = "templates"
    static_path: str = "static"
    web_port: int = 5261
    min_window_width: int = 1024
    min_window_height: int = 576
    default_window_width: int = 1280
    default_window_height: int = 720
    system: str = platform.system()

    linux_directories: SystemDirectories = SystemDirectories(
        scanned_images_path=f"{USER_HOMEDIR}/{LINUX_DATA_DIR}/temp_images/",
        config_dir=f"{USER_HOMEDIR}/{LINUX_DATA_DIR}/config/",
        output_dir=f"{user_dir()}/Pictures/Digitalized/",
        scripts_dir="scripts",
        temp_dir=f"{USER_HOMEDIR}/{LINUX_DATA_DIR}/temp/",
    )
    win_directories: SystemDirectories = SystemDirectories(
        scanned_images_path=f"{APPDATA}\\{WINDOWS_DATA_DIR}\\temp_images\\",
        config_dir=f"{APPDATA}\\{WINDOWS_DATA_DIR}\\config\\",
        output_dir=f"{user_dir()}\\Pictures\\Digitalized\\",
        scripts_dir="scripts\\",
        temp_dir=f"{APPDATA}\\{WINDOWS_DATA_DIR}\\temp\\",
    )

    abs_templates_path = field_validator("templates_path")(get_path)
    abs_static_path = field_validator("static_path")(get_path)

    @property
    def sys_directories(self) -> SystemDirectories:
        return {
            general.System.Linux: self.linux_directories,
            general.System.Windows: self.win_directories,
        }.get(self.system)

    @property
    def path_seperator(self) -> str:
        return {general.System.Linux: "/", general.System.Windows: "\\"}.get(
            self.system
        )

    @property
    def icon_location(self) -> str:
        return f"{self.static_path}/images/favicon.ico"

    @property
    def web_dictionaries_location(self) -> str:
        return f"{self.static_path}/webengine_dictionaries"

    @property
    def is_snap(self) -> bool:
        return os.environ.get("SNAP_NAME") == self.app_name.lower()
