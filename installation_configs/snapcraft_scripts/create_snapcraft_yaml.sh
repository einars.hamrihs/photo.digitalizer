#!/bin/bash
source app_details.sh

cat <<EOF > $SNAP_WORKING_DIR/snapcraft.yaml
type: app
title: $TITLE
name: $PACKAGE_NAME
version: "$VERSION"
summary: $SUMMARY
description: $DESCRIPTION
icon: $SNAP_GUI_DIR/$PACKAGE_NAME.png
grade: stable
confinement: strict
base: core22
contact: $CONTACT_URL
donation: $DONATION_URL
license: $LICENCE_TYPE
source-code: $SOURCE_URL
website: $HELP_URL
architectures:
  - amd64

lint:
  ignore:
    - library

plugs:
  shared-memory:
    private: true
  browser-support:
    allow-sandbox: false

apps:
  $PACKAGE_NAME:
    extensions:
      - gnome
    command: bin/$TITLE
    environment:
      SANE_CONFIG_DIR: \$SNAP/etc/sane.d/
      LD_LIBRARY_PATH: \$SNAP/usr/lib/sane:\$LD_LIBRARY_PATH
      MIBDIRS: \$SNAP/usr/share/snmp/mibs
    plugs:
      - home
      - network
      - shared-memory
      - browser-support
      - raw-usb
      - hardware-observe
      - avahi-observe

parts:
  $PACKAGE_NAME:
    plugin: dump
    source: $PACKAGE_NAME
    organize:
      $TITLE: bin/$TITLE
      _internal: bin/_internal
    stage-snaps:
      - simple-scan
EOF
