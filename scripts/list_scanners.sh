#!/bin/bash

output=$(scanimage -f "%d,%m;")

if [ $? -eq 0 ]; then
  echo "$output"
else
  echo "Error: List scanners failed."
  echo "$output"
fi

exit 0
