import os
from typing import Sequence, Optional

import cv2
import numpy as np
from src.config import BACKGROUND_THRESHOLD

THRESH_BINARY_INV = cv2.THRESH_BINARY_INV
THRESH_BINARY = cv2.THRESH_BINARY


def create_image_array(path: str) -> np.ndarray:
    return cv2.imread(os.path.abspath(path), cv2.IMREAD_UNCHANGED)


def erode(binary_image: np.ndarray, iterations: int) -> np.ndarray:
    kernel = np.ones((5, 5), np.uint8)
    return cv2.erode(binary_image, kernel, iterations=iterations)


def create_binary_image(
    img: np.ndarray,
    min_thresh: Optional[int] = BACKGROUND_THRESHOLD,
    max_thresh: Optional[int] = 255,
    thresh_type: Optional[int] = THRESH_BINARY_INV,
) -> np.ndarray:
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, binary = cv2.threshold(
        src=gray, thresh=min_thresh, maxval=max_thresh, type=thresh_type
    )
    return binary


def get_contours(
    img: np.ndarray,
    erode_iterations: int,
    min_thresh: Optional[int] = BACKGROUND_THRESHOLD,
    max_thresh: Optional[int] = 255,
    thresh_type: Optional[int] = THRESH_BINARY_INV,
) -> Sequence[np.ndarray]:
    binary = create_binary_image(
        img=img, min_thresh=min_thresh, max_thresh=max_thresh, thresh_type=thresh_type
    )
    eroded = erode(binary_image=binary, iterations=erode_iterations)
    contours, _ = cv2.findContours(eroded, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    return contours


def get_rotation_matrix(cntr: np.ndarray) -> tuple[float, np.ndarray]:
    rect = cv2.minAreaRect(cntr)
    angle = rect[-1]
    if angle > 45:
        angle -= 90
    box = cv2.boxPoints(rect)
    box = np.int0(box)
    return angle, box


def rotate_image(img: np.ndarray, angle: float, center: tuple) -> np.ndarray:
    (h, w) = img.shape[:2]
    M = cv2.getRotationMatrix2D(center, angle, 1.0)
    rotated = cv2.warpAffine(
        img, M, (w, h), flags=cv2.INTER_CUBIC, borderMode=cv2.BORDER_REPLICATE
    )
    return rotated


def check_angle_not_straight(
    angle: float,
) -> bool:
    return abs(angle) > 1e-2


def deskew(img: np.ndarray, cntr: np.ndarray) -> np.ndarray:
    angle, box = get_rotation_matrix(cntr)
    if check_angle_not_straight(angle=angle):  # Ignore very small angles
        center = tuple(np.mean(box, axis=0))
        img = rotate_image(img, angle, center)
    return img


def get_bounding_boxes(cnt: np.ndarray) -> Sequence[int]:
    return cv2.boundingRect(cnt)


def get_largest_contour(contours: Sequence[np.ndarray]) -> np.ndarray:
    return max(contours, key=cv2.contourArea)


def write_image_file(img: np.ndarray, path: str) -> None:
    cv2.imwrite(path, img)
