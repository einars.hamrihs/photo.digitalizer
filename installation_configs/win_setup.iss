; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define Name "Pixelyse"
#define PackageName "pixelyse"
#define Version "1.0.0"
#define Publisher "Einārs Hamrihs"
#define ExeName "Pixelyse.exe"
#define SetupName "setup_pixelyse_1.0.0"
#define OutputDir "installation_packages"
#define ReadmePath "README.md"
#define LicencePath "LICENCE.md"
#define FaviconPath "static\images\favicon.ico"

[Setup]
; NOTE: The value of AppId uniquely identifies this application. Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{87E95917-682C-4957-AF38-2B2F8AAB699D}
AppName={#Name}
AppVersion={#Version}
;AppVerName={#Name} {#Version}
AppPublisher={#Publisher}
DefaultDirName={autopf}\{#Name}
; "ArchitecturesAllowed=x64compatible" specifies that Setup cannot run
; on anything but x64 and Windows 11 on Arm.
ArchitecturesAllowed=x64compatible
; "ArchitecturesInstallIn64BitMode=x64compatible" requests that the
; install be done in "64-bit mode" on x64 or Windows 11 on Arm,
; meaning it should use the native 64-bit Program Files directory and
; the 64-bit view of the registry.
ArchitecturesInstallIn64BitMode=x64compatible
DisableProgramGroupPage=yes
; Uncomment the following line to run in non administrative install mode (install for current user only.)
;PrivilegesRequired=lowest
SetupIconFile=..\{#FaviconPath}
Compression=lzma
SolidCompression=yes
WizardStyle=modern
LicenseFile=..\{#LicencePath}
AppReadmeFile=..\{#ReadmePath}
OutputDir=..\{#OutputDir}
OutputBaseFilename={#SetupName}
UninstallDisplayIcon={app}\{#ExeName}

[InstallDelete]
Type: filesandordirs; Name: "{autopf}\{#Name}"

[UninstallDelete]
Type: filesandordirs; Name: "{localappdata}\{#Name}"

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "..\dist\{#PackageName}\{#ExeName}"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\dist\{#PackageName}\_internal\*"; DestDir: "{app}\_internal"; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{autoprograms}\{#Name}"; Filename: "{app}\{#ExeName}"
Name: "{autodesktop}\{#Name}"; Filename: "{app}\{#ExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#ExeName}"; Description: "{cm:LaunchProgram,{#StringChange(Name, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

[Code]
procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
  if CurUninstallStep = usUninstall then
  begin
    // Define the path to your application's executable
    // Replace "YourApp.exe" with your actual executable name
    UnpinShellLink(ExpandConstant('{userappdata}\Microsoft\Internet Explorer\Quick Launch\User Pinned\TaskBar\{#Name}.lnk'));
  end;
end;
