import os
import sys


def get_absolute_path_bundled(relative_path: str) -> str:
    base_path = sys._MEIPASS
    return os.path.join(base_path, relative_path)


def get_path(relative_path: str) -> str:
    if not hasattr(sys, "_MEIPASS"):
        return relative_path
    return get_absolute_path_bundled(relative_path)
