from unittest import mock

from twisted.internet import defer
from twisted.trial import unittest

from src.system_operations.scanner_operator import ScannerError
from src.system_operations.win_scanner_op import (
    POWERSHELL_ARGS,
    LIST_SCANNERS_SCRIPT,
    SCANNED_FILE_PATH,
    POWERSHELL_EXEC,
    WindowsScanner,
    SCAN_SCRIPT,
)
from test.patchers.settings import patch_settings
from test.samples import data_samples

MODULE_PATH = "src.system_operations.win_scanner_op"
POWERSHELL_EXEC = POWERSHELL_EXEC.format(data_samples.WIN_SYSTEM_ROOT)


class TestWindowsScannerOperator(unittest.TestCase):
    def setUp(self):
        patch_settings([MODULE_PATH])
        self.patch_os()
        self.patch_get_process_output()
        self.scanner_operator = WindowsScanner()

    def patch_os(self):
        self.os = mock.AsyncMock()
        self.os.environ = {"SystemRoot": data_samples.WIN_SYSTEM_ROOT}
        patcher = mock.patch(MODULE_PATH + ".os", self.os)
        patcher.start()

    def patch_get_process_output(self):
        self.get_process_output = mock.AsyncMock()
        patcher = mock.patch(MODULE_PATH + ".getProcessOutput", self.get_process_output)
        patcher.start()

    @defer.inlineCallbacks
    def test_list_scanners(self):
        self.get_process_output.return_value = data_samples.LIST_SCANNER_RESPONSE_WIN
        coro = self.scanner_operator.list_scanners()
        result = yield defer.Deferred.fromCoroutine(coro)
        self.get_process_output.assert_called_once_with(
            POWERSHELL_EXEC, [*POWERSHELL_ARGS, LIST_SCANNERS_SCRIPT]
        )
        self.assertEqual(result, [data_samples.SCANNER_MODEL_WIN])

    @defer.inlineCallbacks
    def test_list_scanners_io_error(self):
        self.get_process_output.side_effect = IOError(data_samples.ERROR_TEXT)
        coro = self.scanner_operator.list_scanners()
        with self.assertRaises(ScannerError):
            yield defer.Deferred.fromCoroutine(coro)
            self.get_process_output.assert_called_once_with(
                POWERSHELL_EXEC, [*POWERSHELL_ARGS, LIST_SCANNERS_SCRIPT]
            )

    @defer.inlineCallbacks
    def test_scan_and_save_temp_file(self):
        coro = self.scanner_operator.scan_and_save_temp_file(
            data_samples.SCANNER_DEVICE_NAME
        )
        result = yield defer.Deferred.fromCoroutine(coro)
        self.get_process_output.assert_called_once_with(
            POWERSHELL_EXEC,
            [
                *POWERSHELL_ARGS,
                SCAN_SCRIPT,
                data_samples.SCANNER_DEVICE_NAME,
                SCANNED_FILE_PATH,
            ],
        )
        self.assertIsNone(result)

    @defer.inlineCallbacks
    def test_scan_and_save_temp_file_scan_io_error(self):
        self.get_process_output.side_effect = IOError(data_samples.ERROR_TEXT)
        coro = self.scanner_operator.scan_and_save_temp_file(
            data_samples.SCANNER_DEVICE_NAME
        )
        with self.assertRaises(ScannerError):
            yield defer.Deferred.fromCoroutine(coro)
            self.get_process_output.assert_called_once_with(
                POWERSHELL_EXEC,
                [data_samples.SCANNER_DEVICE_NAME, SCANNED_FILE_PATH],
            )
