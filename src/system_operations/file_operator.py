import os
import shutil
from pathlib import Path

from src.settings import Settings

settings = Settings()


class FileOperator:

    def save_file(self, source_filename: str, dest_filename: str) -> None:
        shutil.copy(
            f"{settings.sys_directories.scanned_images_path}{source_filename}",
            f"{settings.sys_directories.output_dir}{dest_filename}",
        )

    @staticmethod
    def delete_file(file_name: str) -> None:
        file_path = f"{settings.sys_directories.scanned_images_path}{file_name}"
        if os.path.exists(file_path):
            os.remove(file_path)

    @staticmethod
    def list_files(directory: str) -> list[str]:
        return os.listdir(directory)

    @staticmethod
    def remove_dir(directory: str) -> None:
        return shutil.rmtree(directory)

    @staticmethod
    def create_directory(directory: str) -> None:
        Path(directory).mkdir(parents=True, exist_ok=True)

    @staticmethod
    def file_exists(file_path: str) -> bool:
        return os.path.exists(file_path)
