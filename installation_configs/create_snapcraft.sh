#!/bin/bash
source app_details.sh
OUTPUT_FILE="$WORKING_DIR/snapcraft.yaml"
SCRIPTS_DIR="./installation_configs/snapcraft_scripts"
SOURCE_ICON="icon-256x256.png"

mkdir -p $OUTPUT_DIR
mkdir -p $SNAP_WORKING_DIR/$SNAP_GUI_DIR

cp $ICON_PATH/$SOURCE_ICON $SNAP_WORKING_DIR/$SNAP_GUI_DIR/
mv $SNAP_WORKING_DIR/$SNAP_GUI_DIR/$SOURCE_ICON $SNAP_WORKING_DIR/$SNAP_GUI_DIR/$PACKAGE_NAME.png
./$SCRIPTS_DIR/create_snapcraft_yaml.sh
./$SCRIPTS_DIR/create_desktop.sh

cd $SNAP_WORKING_DIR
snapcraft
rm snapcraft.yaml
rm -rf snap
mv *.snap ../$OUTPUT_DIR
