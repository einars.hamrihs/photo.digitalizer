from subprocess import CalledProcessError
from unittest import mock

from twisted.internet import defer
from twisted.trial import unittest

from src.models import general
from src.system_operations.linux_scanner_op import (
    CONVERTED_FILE_PATH,
    SCANNED_FORMAT,
    LIST_SCANNERS_EXEC,
    SCANNED_FILE_NAME,
    SCANNED_FILE_PATH,
    SCAN_EXEC,
    LinuxScanner,
    CHECK_SNAP_INTERFACES,
    SnapInterfaceError,
    CHECK_SNAP_INTERFACES_ON_SCANNING,
)
from src.system_operations.scanner_operator import ScannerError
from test.patchers.settings import patch_settings
from test.samples import data_samples

MODULE_PATH = "src.system_operations.linux_scanner_op"


class TestLinuxScannerOperator(unittest.TestCase):
    def setUp(self):
        self.settings = patch_settings([MODULE_PATH])
        self.settings.is_snap = True
        self.patch_cv2_tools()
        self.patch_check_output()
        self.scanner_operator = LinuxScanner()

    def patch_cv2_tools(self):
        self.cv2_tools = mock.MagicMock()
        patcher = mock.patch(MODULE_PATH + ".cv2_tools", self.cv2_tools)
        patcher.start()

    def patch_check_output(self):
        self.check_output = mock.MagicMock()
        patcher = mock.patch(MODULE_PATH + ".check_output", self.check_output)
        patcher.start()

    @defer.inlineCallbacks
    def test_list_scanners(self):
        self.check_output.side_effect = [None, data_samples.LIST_SCANNER_RESPONSE_LINUX]
        coro = self.scanner_operator.list_scanners()
        result = yield defer.Deferred.fromCoroutine(coro)
        self.check_output.assert_has_calls(
            [mock.call([CHECK_SNAP_INTERFACES]), mock.call([LIST_SCANNERS_EXEC])]
        )
        self.assertEqual(result, [data_samples.SCANNER_MODEL_LINUX])

    @defer.inlineCallbacks
    def test_list_scanners_snap_interface_error(self):
        self.check_output.side_effect = CalledProcessError(
            1,
            CHECK_SNAP_INTERFACES,
            output=general.SnapInterface.avahi_observe.error_code.encode(),
        )
        coro = self.scanner_operator.list_scanners()
        with self.assertRaises(SnapInterfaceError):
            yield defer.Deferred.fromCoroutine(coro)
            self.check_output.assert_called_once_with([CHECK_SNAP_INTERFACES])

    @defer.inlineCallbacks
    def test_list_scanners_list_script_error(self):
        self.settings.is_snap = False
        self.check_output.side_effect = [
            CalledProcessError(1, LIST_SCANNERS_EXEC, data_samples.ERROR_TEXT.encode()),
        ]
        coro = self.scanner_operator.list_scanners()
        with self.assertRaises(ScannerError):
            yield defer.Deferred.fromCoroutine(coro)
            self.check_output.assert_has_calls(
                [
                    mock.call([CHECK_SNAP_INTERFACES]),
                    mock.call([LIST_SCANNERS_EXEC]),
                ]
            )

    @defer.inlineCallbacks
    def test_scan_and_save_temp_file(self):
        coro = self.scanner_operator.scan_and_save_temp_file(
            data_samples.SCANNER_DEVICE_NAME
        )
        result = yield defer.Deferred.fromCoroutine(coro)
        self.check_output.assert_called_once_with(
            [
                SCAN_EXEC,
                data_samples.SCANNER_DEVICE_NAME,
                SCANNED_FORMAT,
                SCANNED_FILE_PATH,
            ],
        )
        self.cv2_tools.create_image_array.assert_called_once_with(SCANNED_FILE_PATH)
        self.cv2_tools.write_image_file.assert_called_once_with(
            img=self.cv2_tools.create_image_array.return_value, path=CONVERTED_FILE_PATH
        )
        self.assertEqual(result, SCANNED_FILE_NAME)

    @defer.inlineCallbacks
    def test_scan_and_save_temp_file_scan_io_error(self):
        self.check_output.side_effect = [
            CalledProcessError(1, SCAN_EXEC, data_samples.ERROR_TEXT.encode()),
            True,
        ]
        coro = self.scanner_operator.scan_and_save_temp_file(
            data_samples.SCANNER_DEVICE_NAME
        )
        with self.assertRaises(ScannerError):
            yield defer.Deferred.fromCoroutine(coro)
            self.check_output.assert_has_calls(
                [
                    mock.call(
                        [
                            SCAN_EXEC,
                            data_samples.SCANNER_DEVICE_NAME,
                            SCANNED_FORMAT,
                            SCANNED_FILE_PATH,
                        ],
                    ),
                    mock.call(
                        [
                            CHECK_SNAP_INTERFACES_ON_SCANNING,
                        ],
                    ),
                ]
            )

    @defer.inlineCallbacks
    def test_scan_and_save_temp_file_iface_not_connected(self):
        self.check_output.side_effect = [
            CalledProcessError(1, SCAN_EXEC, data_samples.ERROR_TEXT.encode()),
            CalledProcessError(
                1,
                CHECK_SNAP_INTERFACES,
                output=general.SnapInterface.raw_usb.error_code.encode(),
            ),
        ]
        coro = self.scanner_operator.scan_and_save_temp_file(
            data_samples.SCANNER_DEVICE_NAME
        )
        with self.assertRaises(ScannerError):
            yield defer.Deferred.fromCoroutine(coro)
            self.check_output.assert_has_calls(
                [
                    mock.call(
                        [
                            SCAN_EXEC,
                            data_samples.SCANNER_DEVICE_NAME,
                            SCANNED_FORMAT,
                            SCANNED_FILE_PATH,
                        ],
                    ),
                    mock.call(
                        [
                            CHECK_SNAP_INTERFACES_ON_SCANNING,
                        ],
                    ),
                ]
            )

    @defer.inlineCallbacks
    def test_scan_and_save_temp_file_iface_check_error(self):
        self.check_output.side_effect = [
            CalledProcessError(1, SCAN_EXEC, data_samples.ERROR_TEXT.encode()),
            CalledProcessError(
                1, CHECK_SNAP_INTERFACES, output=data_samples.ERROR_TEXT.encode()
            ),
        ]
        coro = self.scanner_operator.scan_and_save_temp_file(
            data_samples.SCANNER_DEVICE_NAME
        )
        with self.assertRaises(SnapInterfaceError):
            yield defer.Deferred.fromCoroutine(coro)
            self.check_output.assert_has_calls(
                [
                    mock.call(
                        [
                            SCAN_EXEC,
                            data_samples.SCANNER_DEVICE_NAME,
                            SCANNED_FORMAT,
                            SCANNED_FILE_PATH,
                        ],
                    ),
                    mock.call(
                        [
                            CHECK_SNAP_INTERFACES_ON_SCANNING,
                        ],
                    ),
                ]
            )
