#!/bin/bash
source app_details.sh

# Create the post-installation script
cat <<EOF > $PACKAGE_DIR/DEBIAN/postinst
#!/bin/bash
# Make sure the desktop entry has correct permissions
chmod 644 /usr/share/applications/$PACKAGE_NAME.desktop
for size in ${ICON_SIZES[@]}; do
  chmod 644 /usr/share/icons/hicolor/\${size}x\${size}/apps/$PACKAGE_NAME.png
done
update-desktop-database
EOF
chmod 755 $PACKAGE_DIR/DEBIAN/postinst
