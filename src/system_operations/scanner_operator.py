from abc import ABC, abstractmethod
from typing import Optional

from src.models import data_models

SCANNED_FORMAT = "png"
SCANNED_FILENAME = "scanned"


class ScannerError(Exception):
    pass


class ScannerOperator(ABC):
    @abstractmethod
    async def scan_and_save_temp_file(self, device_name: str) -> str:
        raise NotImplementedError

    @abstractmethod
    async def list_scanners(self) -> list[data_models.ScannerData]:
        raise NotImplementedError

    @staticmethod
    async def check_snap_scanning_interfaces() -> Optional[bool]:  # pragma: no cover
        raise NotImplementedError
