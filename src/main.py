import sys

from PyQt6.QtWidgets import QApplication
from klein import Klein
from twisted.internet import defer
from twisted.internet import threads
from twisted.internet.endpoints import TCP4ServerEndpoint
from twisted.web.server import Site

from src.repository import Repository
from src.settings import Settings
from src.system_operations.factory import scanner_operator_factory
from src.web_app.web_app_controller import WebAppController
from src.window import MainWindow

settings = Settings()


def run_pyqt(repo: Repository) -> None:
    qt_app = QApplication(sys.argv)
    window = MainWindow(repo)
    window.show()
    sys.exit(qt_app.exec())


async def main(reactor):
    app = Klein()
    app_deferred = defer.Deferred()

    repo = Repository(
        app_deferred=app_deferred,
        scanner_operator=scanner_operator_factory(),
    )
    repo.start()
    web_app = WebAppController(app=app, app_deferred=app_deferred)
    web_app.start(repo=repo)

    site = Site(app.resource())
    server_ep = TCP4ServerEndpoint(reactor, settings.web_port)
    threads.deferToThread(run_pyqt, repo)
    await server_ep.listen(site)

    await app_deferred
