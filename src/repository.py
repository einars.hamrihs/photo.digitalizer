import re
from typing import Optional

from twisted.internet import defer
from twisted.logger import Logger

from src.config import Config
from src.models import data_models, request_models
from src.photo_extractor.photo_extractor import (
    PHOTO_FILENAME_PREFIX,
    PhotoExtractor,
    OUTPUT_FORMAT,
)
from src.settings import Settings
from src.system_operations.file_operator import FileOperator
from src.system_operations.scanner_operator import (
    ScannerOperator,
    SCANNED_FILENAME,
    SCANNED_FORMAT,
)
from src.system_operations.linux_scanner_op import SnapInterfaceError

settings = Settings()


class Repository:
    log = Logger()

    def __init__(
        self,
        app_deferred: defer.Deferred,
        scanner_operator: ScannerOperator,
    ):
        self.scanners: Optional[list[data_models.ScannerData]] = None
        self.scanner_operator = scanner_operator
        self.file_operator = FileOperator()
        self.app_deferred = app_deferred
        self.config = Config()

    def start(self) -> None:
        self.file_operator.create_directory(
            settings.sys_directories.scanned_images_path
        )
        self.file_operator.create_directory(settings.sys_directories.output_dir)

    @property
    def config_serialized(self) -> dict:
        return self.config.model_dump()

    async def set_scanners(self):
        try:
            self.scanners = await self.scanner_operator.list_scanners()
        except SnapInterfaceError as e:
            self.scanners = []
            raise e
        self.log.debug("Updated list of scanners: {scanners}", scanners=self.scanners)

    def reset_scanners(self) -> None:
        self.scanners = None

    @property
    def scanners_updated(self) -> bool:
        return isinstance(self.scanners, list)

    @property
    def no_scanners_found(self) -> bool:
        return self.scanners_updated and len(self.scanners) == 0

    def scanner_exists(self, device_name: str) -> bool:
        if self.scanners_updated:
            return device_name in [scanner.device_name for scanner in self.scanners]

    async def check_snap_scanning_interfaces(self) -> bool:
        return await self.scanner_operator.check_snap_scanning_interfaces()

    async def list_scanners_serialized(
        self, reload: bool = False, reset: bool = False
    ) -> Optional[list[dict]]:
        if reset:
            return self.reset_scanners()
        if reload:
            await self.set_scanners()
        return (
            [scanner.model_dump() for scanner in self.scanners]
            if self.scanners_updated
            else None
        )

    def update_extract_config(self, scan_request: request_models.ScanRequest) -> None:
        self.config.update_config(
            background_threshold=scan_request.background_threshold,
            min_aspect_ratio=scan_request.min_aspect_ratio,
            max_aspect_ratio=scan_request.max_aspect_ratio,
            min_photo_area=scan_request.min_photo_area,
            erode_iterations=scan_request.erode_iterations,
        )

    def extract_photos(self) -> None:
        photo_extractor = PhotoExtractor(self.config)
        photo_extractor.extract_photos()

    async def scan_and_extract_photos(
        self, scan_request: request_models.ScanRequest
    ) -> None:
        self.update_extract_config(scan_request)
        self.file_operator.delete_file(f"{SCANNED_FILENAME}.{SCANNED_FORMAT}")
        temp_file = await self.scanner_operator.scan_and_save_temp_file(
            device_name=scan_request.device_name
        )
        if temp_file:
            self.file_operator.delete_file(temp_file)
        self.extract_photos()

    def get_image_file_list(
        self,
        directory: Optional[str] = settings.sys_directories.scanned_images_path,
        prefix: Optional[str] = PHOTO_FILENAME_PREFIX,
    ) -> list[str]:
        photos_list = [
            file
            for file in self.file_operator.list_files(directory)
            if file.startswith(prefix)
        ]
        photos_list.sort()
        return photos_list

    @staticmethod
    def get_number_from_filename(filename: str) -> int:
        parts = re.split("[_|.]", filename)
        return int(parts[-2]) if len(parts) > 1 else 0

    def create_filename(self, prefix: str) -> str:
        existing_files = self.get_image_file_list(
            directory=settings.sys_directories.output_dir, prefix=prefix
        )
        last_number = (
            self.get_number_from_filename(existing_files[-1]) if existing_files else 0
        )
        return f"{prefix}_{last_number + 1:05d}.{OUTPUT_FORMAT}"

    def save_file(self, source_filename: str, filename_prefix: str) -> None:
        dest_filename = self.create_filename(filename_prefix)
        self.file_operator.save_file(
            source_filename=source_filename, dest_filename=dest_filename
        )

    def save_file_by_index(self, saved_index: int, filename_prefix: str) -> None:
        source_filename = self.get_image_file_list()[saved_index]
        self.save_file(source_filename=source_filename, filename_prefix=filename_prefix)

    def save_all_files(self, filename_prefix: str) -> None:
        for image_index, source_filename in enumerate(self.get_image_file_list()):
            self.save_file(
                source_filename=source_filename, filename_prefix=filename_prefix
            )

    @staticmethod
    def get_next_image_to_show(index: int, input_list: list) -> Optional[int]:
        if not input_list:
            return
        return index if len(input_list) == index + 1 else index - 1

    def delete_file(self, image_index: int) -> Optional[int]:
        file_name = self.get_image_file_list()[image_index]
        self.file_operator.delete_file(file_name)
        return self.get_next_image_to_show(
            index=image_index, input_list=self.get_image_file_list()
        )

    def delete_all_files(self) -> None:
        for file_name in self.get_image_file_list():
            self.file_operator.delete_file(file_name)

    def reset_settings(self) -> None:
        self.config.update_config()

    def close(self) -> None:
        self.log.info("Closing")
        self.file_operator.remove_dir(settings.sys_directories.scanned_images_path)
        self.file_operator.remove_dir(settings.sys_directories.temp_dir)
        self.app_deferred.callback(None)

    @property
    def temp_file_exists(self) -> bool:
        return self.file_operator.file_exists(
            f"{settings.sys_directories.scanned_images_path}{SCANNED_FILENAME}.{SCANNED_FORMAT}"
        )
