import os

from src.config import (
    BACKGROUND_THRESHOLD,
    MAX_ASPECT_RATIO,
    MIN_ASPECT_RATIO,
    MIN_PHOTO_AREA,
    ERODE_ITERATIONS,
)
from src.models import request_models
from src.models.data_models import ScannerData
from src.system_operations.scanner_operator import SCANNED_FILENAME, SCANNED_FORMAT


def full_path(path: str) -> str:
    return f"{os.getcwd()}/{path}".replace("_trial_temp/", "")


def form_str_from_dict(form_dict: dict) -> str:
    return "&".join([f"{key}={val}" for key, val in form_dict.items()])


SCANNER_DEVICE_NAME = "scanner:device"
SCANNER_TITLE = "Test Scanner"
LIST_SCANNER_RESPONSE_LINUX = f"{SCANNER_DEVICE_NAME},{SCANNER_TITLE}".encode()
LIST_SCANNER_RESPONSE_WIN = f"{SCANNER_DEVICE_NAME}".encode()
USER_DIR = "/test"
TIMESTAMP_1 = "1718281631.9494438"
TIMESTAMP_2 = "1718281631.9494439"
TEMP_FILENAME1 = f"photo_{TIMESTAMP_1}.png"
TEMP_FILENAME2 = f"photo_{TIMESTAMP_2}.png"
OUTPUT_PREFIX = "digitalized"
OUTPUT_FILENAME1 = f"{OUTPUT_PREFIX}_00001.png"
OUTPUT_FILENAME2 = f"{OUTPUT_PREFIX}_00002.png"
OUTPUT_FILENAME3 = f"{OUTPUT_PREFIX}_00003.png"
OUTPUT_FILENAME4 = f"{OUTPUT_PREFIX}_00004.png"
ERROR_TEXT = "error"
SCANNED_IMAGES_PATH = "/tmp/Pixelyse/temp_images/"
TEMP_PATH = "/home/user/.Pixelyse/temp/"
SCANNED_TEMP_FILE_PATH = f"{SCANNED_IMAGES_PATH}{SCANNED_FILENAME}.{SCANNED_FORMAT}"
OUTPUT_DIR_LINUX = "/home/user/Pictures/"
PHOTO_1_PATH = full_path("test/data/example_1/")
PHOTO_2_PATH = full_path("test/data/example_2/")
APP_NAME = "test app"
PHOTO_INDEX = 0
WIN_SYSTEM_ROOT = "C:\\Windows\\System32"
CUSTOM_FILENAME_PREFIX = "pic"

SETTINGS = {
    "app_name": APP_NAME,
    "path_seperator": "/",
    "system": "Linux",
    "is_snap": False,
}
SYS_DIRECTORIES = {
    "scanned_images_path": SCANNED_IMAGES_PATH,
    "output_dir": OUTPUT_DIR_LINUX,
    "temp_dir": TEMP_PATH,
}
SCANNER_DATA_LINUX = {
    "device_name": SCANNER_DEVICE_NAME,
    "title": SCANNER_TITLE,
}
SCANNER_DATA_WIN = {
    "device_name": SCANNER_DEVICE_NAME,
    "title": SCANNER_DEVICE_NAME,
}
SCANNER_MODEL_LINUX = ScannerData(**SCANNER_DATA_LINUX)
SCANNER_MODEL_WIN = ScannerData(**SCANNER_DATA_WIN)
PHOTO_EXTRACT_PARAMS = {
    "background_threshold": BACKGROUND_THRESHOLD,
    "min_aspect_ratio": MIN_ASPECT_RATIO,
    "max_aspect_ratio": MAX_ASPECT_RATIO,
    "min_photo_area": MIN_PHOTO_AREA,
    "erode_iterations": ERODE_ITERATIONS,
}
LISTDIR_RESPONSE_TEMP = [TEMP_FILENAME1, TEMP_FILENAME2]
LISTDIR_RESPONSE_TEMP_AFTER_DELETE = [TEMP_FILENAME1]
LISTDIR_RESPONSE_OUTPUT = [OUTPUT_FILENAME1, OUTPUT_FILENAME2]
LISTDIR_RESPONSE_OUTPUT_AFTER_SAVE = [
    OUTPUT_FILENAME1,
    OUTPUT_FILENAME2,
    OUTPUT_FILENAME3,
]
SCAN_REQUEST = {"device_name": SCANNER_DEVICE_NAME, **PHOTO_EXTRACT_PARAMS}
SCAN_REQUEST_FORM = form_str_from_dict(SCAN_REQUEST)
UPDATE_ACCORDION_REQUEST = {"status": request_models.ACCORDION_SHOWN}
UPDATE_ACCORDION_FORM = form_str_from_dict(UPDATE_ACCORDION_REQUEST)
SAVE_ALL_REQUEST = {"filename_prefix": request_models.PHOTO_FILENAME_PREFIX}
SAVE_ALL_REQUEST_FORM = form_str_from_dict(SAVE_ALL_REQUEST)
SAVE_REQUEST = {"saved_index": PHOTO_INDEX, **SAVE_ALL_REQUEST}
SAVE_REQUEST_FORM = form_str_from_dict(SAVE_REQUEST)
UPDATE_SAVE_CONFIG = {**SAVE_REQUEST, "filename_prefix": CUSTOM_FILENAME_PREFIX}
UPDATE_SAVE_CONFIG_FORM = form_str_from_dict(UPDATE_SAVE_CONFIG)
DELETE_INDEX = PHOTO_INDEX + 1
DELETE_REQUEST = {
    "image_index": DELETE_INDEX,
}
DELETE_REQUEST_FORM = form_str_from_dict(DELETE_REQUEST)
PAGE_RENDER_PARAMS = {
    "title": APP_NAME,
    "alerts": [],
    "advanced_params": PHOTO_EXTRACT_PARAMS,
    "scanners": [SCANNER_DATA_LINUX],
    "image_list": [],
    "image_index": PHOTO_INDEX,
    "selected_scanner": None,
    "accordion": False,
    "temp_file_exists": True,
    "gui_filename_prefix": None,
    "scanners_updated": True,
}
MAIN_QUERY_ARGS = {"image_index".encode(): [PHOTO_INDEX]}
