from enum import StrEnum
from typing import Optional

from pydantic import BaseModel, field_serializer

LOADING_SCANNERS = "Loading scanners"


class AlertType(StrEnum):
    danger = "danger"
    warning = "warning"
    success = "success"


class Alert(BaseModel):
    type: AlertType = AlertType.danger
    content: str

    @field_serializer("content")
    def serialize_content(self, content: str) -> str:
        return content.replace("\n", "<br>")


class PageParams(BaseModel):
    alerts: Optional[list[Alert]] = None


class MainParams(PageParams):
    advanced_params: dict
    scanners: Optional[list[dict]] = None
    image_list: Optional[list[str]] = None
    image_index: Optional[int] = None
    selected_scanner: Optional[str] = None
    accordion: Optional[bool] = False
    temp_file_exists: Optional[bool] = False
    gui_filename_prefix: Optional[str] = None
    scanners_updated: bool
