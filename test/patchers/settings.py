from unittest import mock

from test.samples import data_samples


def patch_settings(
    locations: list[str],
    settings: dict = data_samples.SETTINGS,
    system_directories: dict = data_samples.SYS_DIRECTORIES,
) -> mock.MagicMock:
    settings_mock = mock.MagicMock(
        **settings, sys_directories=mock.MagicMock(**system_directories)
    )
    for location in locations:
        patcher = mock.patch(location + ".settings", settings_mock)
        patcher.start()
    return settings_mock
