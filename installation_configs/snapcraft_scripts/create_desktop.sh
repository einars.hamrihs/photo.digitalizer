#!/bin/bash
source app_details.sh

cat <<EOF > $SNAP_WORKING_DIR/$SNAP_GUI_DIR/$PACKAGE_NAME.desktop
[Desktop Entry]
Version=$VERSION
Name=$TITLE
Comment=$DESCRIPTION
Exec=$PACKAGE_NAME
Icon=\${SNAP}/meta/gui/$PACKAGE_NAME.png
Terminal=false
Type=Application
Categories=Utility;
StartupWMClass=$EXEC_FILE
EOF
