# pixelyse
<p>
  <img src="./static/images/logo.png" alt="logo" width="128" height="128" style="float: right; margin-left: 10px;">
  A simple tool for scanning and digitalizing your photos effortlessly. Minimizes the number of steps required to perform scanning and photo extraction, making the process quicker and more efficient. A user-friendly interface and streamlined functionality allows you to go from document to extracted photos in just a few clicks.
</p>

## Technical information
### Description
A python `twisted`, `Klein` and `Jinja2` based backend web server with `PyQt6`'s `QWebEngineView` based GUI. The styling on the web interface is using Bootstrap 5.3.

Uses Linux `scanimage` and Windows `WIA.DeviceManager` to list scanners and scan a document. After scanning `cv2` is used to find photographs by determining the background and finding contours that are big enough and have photo appropriate dimensions. If the photographs are not placed in the scanner straight, they are automatically deskewed and borders are removed.

Created and tested on python3.12. Might not work on older versions of python.

On Linux tested on Ubuntu 22.04. Can be run from the source or compiled on other distributions as long as they can run `PyQt6` and have `scanimage` installed.

### Directories
The tool creates temporary directories to store scanned temporary images and photo extraction configuration .yaml file.

On Windows the temporary directory is located in `%LocalAppData%\Pixelyse`

On Linux the temporary directory is located in `~/.Pixelyse/`

## How to run
### from the source
Create a virtual environment in the project's directory and install the requirements:
#### on Linux:
> python3.12 -m venv venv
>
> source venv/bin/activate
>
> pip install -r requirements.txt
>
> python launcher.py

#### on Windows:
> python -m venv venv
>
> source venv\Scripts\activate
>
> pip install -r requirements.txt
>
> pip install -r requirements-win.txt
>
> python launcher.py

### Install
Use the precompiled installation from the releases page:

[ixelyse / releases](https://gitlab.com/einars.hamrihs/pixelyse/-/releases).

or create the installation yourself:

> pyinstaller inst.spec

This will create executable and other files inside `dist` directory in the project.

On Windows, use the `win_setup.iss` file to create an installation setup with Inno setup.

On Ubuntu or other Debian based Linux, run the `./installation_configs/create_deb.sh` script from project's root directory to create a `.deb` installation package or run `./installation_configs/create_snapcraft.sh` to create a `.snap` package.

## Usage
The scanning process is quite simple.

Place the photos in the scanner with some space in between so the tool could differentiate each photo. If you want to avoid having to rotate the images after scanning, make sure you are placing the photos according to the scanner's page orientation (i. e. the way you would usually place a document on the scanner).

Then choose the scanner and press the `scan` button. Note that the tool scans only one page at a time.

Once the scanning is done, you will see a preview of the photos found.

If any of the images in the preview are not as expected, you can delete them from temporary storage and save the good ones.

Save the photos you want to keep in the `<User directory>/Pictures/Digitalized` folder either one by one or all at once. You can define the prefix for the exported files. For example, if you enter a prefix `digitalized`, then the image will be saved with a filename `digitalized_x.png` with x being sequence number for the photo in the output directory. The default prefix is `img`.

The results can vary for different scanners. If you're noticing that the tool is not finding photos or is only cutting out parts of them, you can adjust the settings:
* `background threshold` - adjust the value 0-255 to find the background for the scanned document (usually white). The darker your background is, the lower value you need.
* `min photo area` - defined in px to avoid extracting smaller parts of the photo with square contours. If you're using a scanner with higher dpi, you can increase this, as the photographies are going to have a higher dpi.
* `min/max aspect ratio` - minimum and maximum value for width / height considering that photos can be vertical and horizontal. These parameters help to avoid extracting parts of the pictures that might look like a square but are actually long, narrow strips.
* `erode iterations` - the erosion parameter is used to avoid getting photos with white borders after deskewing. If the border in the result is too visible, increase the iterations. Decrease it if it's cutting off too much from the picture.

After adjusting the settings you can try to extract photos again by pressing the `Reextract` button. This way you won't have to wait for the whole scanning process to be completed again as this method uses the previously scanned image.

## Common Issues

### Bright photos or photos with bright parts are not found at all or have the bright parts cut off

**Solution:** Increase the background threshold level, so the tool can differentiate the picture from the scanner's white background

### Photos have white borders

**Solution:** Increase the erode iteration and try to extract again until you don't see the border in the result

### Only a part from the photography is extracted

**Solution:** Decrease the erode iteration and try to extract again until you see the full photography

### No scanners are found using snap package, but are found in other scanning applications

**Solution:** The required snap interfaces could not be enabled automatically. To enable them, run these commands:
>snap connect pixelyse:avahi_observe
>
>snap connect pixelyse:raw_usb
>
>snap connect pixelyse:hardware_observe

or open the Pixelyse page in the Snap store, click `permissions` and assign all the permissions available there.

If your scanner is still not found, you can try to install `.deb` package version and report a bug.

### 'Scanner busy' error on Windows while nothing else is using the scanner

**Solution:** The previous scan might have been interrupted. Restart the computer and you should be fine. 

## Testing
Run tests with:
> coverage run -m twistd.trial test
>
> coverage report -m

## Licence
[Licence](LICENCE.md)
