#!/bin/bash
source app_details.sh

# Copy the application files
cp -r $APP_EXEC_DIR/* $PACKAGE_DIR$INSTALL_DIR
for size in "${ICON_SIZES[@]}"; do
  cp $ICON_PATH/icon-${size}x${size}.png $PACKAGE_DIR/usr/share/icons/hicolor/${size}x${size}/apps/$PACKAGE_NAME.png
done
