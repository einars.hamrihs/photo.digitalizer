from pydantic import BaseModel


SCANIMAGE_DETAILS_SEPERATOR = ","


class ScannerData(BaseModel):
    device_name: str
    title: str

    @classmethod
    def create_from_scanimage(cls, scanner_line: str) -> "ScannerData":
        details = scanner_line.split(SCANIMAGE_DETAILS_SEPERATOR)
        return cls(device_name=details[0], title=details[1])

    @staticmethod
    def scanimage_valid_details(scanner_line: str) -> bool:
        return len(scanner_line.split(SCANIMAGE_DETAILS_SEPERATOR)) > 1

    @classmethod
    def create_from_wia(cls, name: str) -> "ScannerData":
        return cls(device_name=name, title=name)

    @staticmethod
    def wia_valid_details(name: str) -> bool:
        return bool(name)
