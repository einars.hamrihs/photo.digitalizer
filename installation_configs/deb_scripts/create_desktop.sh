#!/bin/bash
source app_details.sh

# Create the desktop entry
cat <<EOF > $PACKAGE_DIR/usr/share/applications/$PACKAGE_NAME.desktop
[Desktop Entry]
Version=$VERSION
Name=$TITLE
Comment=$DESCRIPTION
Exec=$INSTALL_DIR/$EXEC_FILE
Icon=$PACKAGE_NAME
Terminal=false
Type=Application
Categories=Utility;
StartupWMClass=$EXEC_FILE
EOF
