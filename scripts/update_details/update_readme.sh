#!/bin/bash
source app_details.sh

SCRIPTS_DIR="scripts/update_details"
README="README.md"

NEW_CONTENT=$(cat <$SCRIPTS_DIR/readme_intro_template.md)
NEW_CONTENT=${NEW_CONTENT//PACKAGE_NAME/$PACKAGE_NAME}
NEW_CONTENT=${NEW_CONTENT//SUMMARY/$SUMMARY}
NEW_CONTENT=${NEW_CONTENT//DESCRIPTION/$DESCRIPTION}

TEMP_FILE=$(mktemp)
echo "$NEW_CONTENT
" > "$TEMP_FILE"

sed -i "/^# .*$/,/^$/{
  /^$/!d
  r $TEMP_FILE
  /^$/d
}" "$README"

rm "$TEMP_FILE"
