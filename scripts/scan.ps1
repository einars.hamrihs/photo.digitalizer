param (
    [string]$scannerName,
    [string]$outputFilePath
)

$wiaManager = new-object -ComObject WIA.DeviceManager
$devices = $wiaManager.DeviceInfos
$scanner = $null

foreach ($device in $devices) {
    if ($device.Properties("Name").Value -eq $scannerName) {
        $scanner = $device.Connect()
        break
    }
}

if ($scanner -eq $null) {
    throw "Scanner '$scannerName' not found."
}

$wiaFormatPNG = "{B96B3CAF-0728-11D3-9D7B-0000F81EF32E}"
foreach ($item in $scanner.Items) {
    $image = $item.Transfer($wiaFormatPNG)
}

if($image.FormatID -ne $wiaFormatPNG)
{
    $imageProcess = new-object -ComObject WIA.ImageProcess

    $imageProcess.Filters.Add($imageProcess.FilterInfos.Item("Convert").FilterID)
    $imageProcess.Filters.Item(1).Properties.Item("FormatID").Value = $wiaFormatPNG
    $image = $imageProcess.Apply($image)
}

$image.SaveFile("$outputFilePath")
