#!/bin/bash
source app_details.sh

# Create the directory structure for the package
mkdir -p $PACKAGE_DIR/DEBIAN
mkdir -p $PACKAGE_DIR/$INSTALL_DIR
mkdir -p $PACKAGE_DIR/usr/share/applications
for size in "${ICON_SIZES[@]}"; do
  mkdir -p $PACKAGE_DIR/usr/share/icons/hicolor/${size}x${size}/apps
done
