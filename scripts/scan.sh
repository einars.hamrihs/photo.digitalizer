#!/bin/bash

if [ $# -ne 3 ]; then
  echo "Error: Please provide device, format and output file arguments."
  echo "Usage: $0 <DEVICE> <FORMAT> <OUTPUT_FILE>"
  exit 1
fi

device="$1"
format="$2"
output_file="$3"

scanimage -d "$device" --format=$format >$output_file

if [ $? -eq 0 ]; then
  echo "Success"
else
  echo "Error: Scan failed. Check scanner and device configuration." && exit 1
fi

exit 0
