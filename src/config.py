import os
from typing import Type

import yaml
from pydantic_settings import (
    SettingsConfigDict,
    BaseSettings,
    YamlConfigSettingsSource,
    PydanticBaseSettingsSource,
)

from src.settings import Settings

settings = Settings()

MIN_ASPECT_RATIO = 0.5
MAX_ASPECT_RATIO = 2
MIN_PHOTO_AREA = 200000
BACKGROUND_THRESHOLD = 200
ERODE_ITERATIONS = 3
YAML_FILE = "config.yaml"
CONFIG_PATH = f"{settings.sys_directories.config_dir}/{YAML_FILE}"


class Config(BaseSettings):
    min_aspect_ratio: float = MIN_ASPECT_RATIO
    max_aspect_ratio: float = MAX_ASPECT_RATIO
    min_photo_area: int = MIN_PHOTO_AREA
    background_threshold: int = BACKGROUND_THRESHOLD
    erode_iterations: int = ERODE_ITERATIONS

    model_config = SettingsConfigDict(
        yaml_file=CONFIG_PATH,
    )

    @staticmethod
    def create_config_dir() -> None:
        if not os.path.exists(settings.sys_directories.config_dir):
            os.makedirs(settings.sys_directories.config_dir)

    @classmethod
    def settings_customise_sources(
        cls,
        settings_cls: Type[BaseSettings],
        init_settings: PydanticBaseSettingsSource,
        env_settings: PydanticBaseSettingsSource,
        dotenv_settings: PydanticBaseSettingsSource,
        file_secret_settings: PydanticBaseSettingsSource,
    ) -> tuple[PydanticBaseSettingsSource, ...]:
        if not os.path.isfile(CONFIG_PATH):
            cls.create_config_dir()
        return (YamlConfigSettingsSource(settings_cls),)

    def update_config(
        self,
        min_aspect_ratio: float = MIN_ASPECT_RATIO,
        max_aspect_ratio: float = MAX_ASPECT_RATIO,
        min_photo_area: int = MIN_PHOTO_AREA,
        background_threshold: int = BACKGROUND_THRESHOLD,
        erode_iterations: int = ERODE_ITERATIONS,
    ) -> None:
        self.min_aspect_ratio = min_aspect_ratio
        self.max_aspect_ratio = max_aspect_ratio
        self.min_photo_area = min_photo_area
        self.background_threshold = background_threshold
        self.erode_iterations = erode_iterations
        file = open(CONFIG_PATH, "w")
        yaml.dump(self.model_dump(), file)
