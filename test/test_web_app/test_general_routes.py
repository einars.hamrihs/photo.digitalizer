from unittest import mock

from twisted.internet import defer
from twisted.internet.defer import inlineCallbacks
from twisted.trial import unittest

from src.models import general, page_params, request_models
from src.system_operations.scanner_operator import ScannerError
from src.web_app.routes.general import (
    DELETED_ALERT,
    GeneralRoutes,
    NO_SCANNERS_FOUND,
    SAVED_TO,
    BACKEND_ERROR_ALERT,
    DEFAULT_SETTINGS_RESTORED_ALERT,
    SnapInterfaceError,
    NO_SCANNERS_FOUND_SNAP,
)
from test.patchers.settings import patch_settings
from test.samples import data_samples
from src.photo_extractor.photo_extractor import PhotoExtractError

MODULE_PATH = "src.web_app.routes.general"
ROUTES_BASE_PATH = "src.web_app.routes.default"


class TestGeneralRoutes(unittest.TestCase):
    def setUp(self):
        self.settings = patch_settings([MODULE_PATH, ROUTES_BASE_PATH])
        self.patch_jinja()
        self.create_request()
        self.patch_repo()
        self.patch_file()
        self.patch_redirect()
        self.general_routes = GeneralRoutes(
            repo=self.repo,
        )

    def tearDown(self):
        self.page.reset_mock()

    def create_request(self):
        self.request = mock.MagicMock()
        self.request.method = general.RESTMethod.GET.encode()

    def add_form_content(self, content: bytes):
        self.request.method = general.RESTMethod.POST.encode()
        self.request.content.getvalue.return_value = content
        self.request.getHeader.return_value = (
            general.ContentType.x_www_form_urlencoded.encode()
        )

    def patch_repo(self):
        self.repo = mock.MagicMock()
        self.repo.config_serialized = data_samples.PHOTO_EXTRACT_PARAMS
        self.repo.list_scanners_serialized = mock.AsyncMock(
            return_value=[data_samples.SCANNER_DATA_LINUX]
        )
        self.repo.scan_and_extract_photos = mock.AsyncMock()
        self.repo.delete_file.return_value = data_samples.PHOTO_INDEX
        self.repo.no_scanners_found = False
        self.repo.scanner_exists.return_value = True
        self.repo.check_snap_scanning_interfaces = mock.AsyncMock(return_value=True)

    def patch_jinja(self):
        self.page = mock.MagicMock()
        jinja = mock.MagicMock()
        jinja.get_template.return_value = self.page
        patcher = mock.patch(
            ROUTES_BASE_PATH + ".jinja2.Environment", return_value=jinja
        )
        patcher.start()

    def patch_file(self):
        self.file = mock.MagicMock()
        patcher = mock.patch(MODULE_PATH + ".File", self.file)
        patcher.start()

    def patch_redirect(self):
        self.redirect = mock.MagicMock()
        patcher = mock.patch(MODULE_PATH + ".Redirect", self.redirect)
        patcher.start()

    @inlineCallbacks
    def test_main(self):
        self.request.path = general.AppPath.main.path.encode()
        self.request.args = data_samples.MAIN_QUERY_ARGS
        coro = self.general_routes.main(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.page.render.assert_called_with(**data_samples.PAGE_RENDER_PARAMS)

    @inlineCallbacks
    def test_main_snap_interface_error(self):
        self.request.path = general.AppPath.main.path.encode()
        self.repo.list_scanners_serialized.side_effect = SnapInterfaceError(
            data_samples.ERROR_TEXT
        )
        self.repo.no_scanners_found = True
        coro = self.general_routes.main(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "alerts": [
                    page_params.Alert(
                        type=page_params.AlertType.danger,
                        content=data_samples.ERROR_TEXT,
                    ).model_dump(),
                    page_params.Alert(
                        type=page_params.AlertType.danger, content=NO_SCANNERS_FOUND
                    ).model_dump(),
                ],
                "scanners": None,
            }
        )

    @inlineCallbacks
    def test_main_no_scanners_found(self):
        self.request.path = general.AppPath.main.path.encode()
        self.repo.list_scanners_serialized.return_value = []
        self.repo.no_scanners_found = True
        coro = self.general_routes.main(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "alerts": [
                    page_params.Alert(
                        type=page_params.AlertType.danger, content=NO_SCANNERS_FOUND
                    ).model_dump()
                ],
                "scanners": [],
            }
        )

    @inlineCallbacks
    def test_main_no_scanners_found_iface_not_connected(self):
        self.settings.is_snap = True
        self.repo.check_snap_scanning_interfaces.return_value = False
        self.request.path = general.AppPath.main.path.encode()
        self.repo.list_scanners_serialized.return_value = []
        self.repo.no_scanners_found = True
        coro = self.general_routes.main(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "alerts": [
                    page_params.Alert(
                        type=page_params.AlertType.danger,
                        content=NO_SCANNERS_FOUND_SNAP,
                    ).model_dump()
                ],
                "scanners": [],
            }
        )

    @inlineCallbacks
    def test_scan(self):
        self.add_form_content(data_samples.SCAN_REQUEST_FORM.encode())
        self.request.path = general.AppPath.scan.path.encode()
        self.repo.scanner_exists.return_value = False
        coro = self.general_routes.scan(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.repo.delete_all_files.assert_called_once()
        self.repo.scan_and_extract_photos.assert_called_with(
            request_models.ScanRequest(**data_samples.SCAN_REQUEST)
        )
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "selected_scanner": None,
            }
        )

    @inlineCallbacks
    def test_scan_scanner_error(self):
        self.add_form_content(data_samples.SCAN_REQUEST_FORM.encode())
        self.request.path = general.AppPath.scan.path.encode()
        self.repo.scan_and_extract_photos.side_effect = ScannerError(
            data_samples.ERROR_TEXT
        )
        coro = self.general_routes.scan(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.repo.delete_all_files.assert_called_once()
        self.repo.scan_and_extract_photos.assert_called_with(
            request_models.ScanRequest(**data_samples.SCAN_REQUEST)
        )
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "selected_scanner": data_samples.SCANNER_DEVICE_NAME,
                "alerts": [
                    page_params.Alert(
                        type=page_params.AlertType.danger,
                        content=data_samples.ERROR_TEXT,
                    ).model_dump()
                ],
            }
        )

    @inlineCallbacks
    def test_re_extract_photos(self):
        self.request.path = general.AppPath.reextract.path.encode()
        coro = self.general_routes.reextract_photos(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.repo.delete_all_files.assert_called_once()
        self.repo.extract_photos.assert_called_once()
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "selected_scanner": None,
            }
        )

    @inlineCallbacks
    def test_re_extract_photos_photo_extract_error(self):
        self.repo.extract_photos.side_effect = PhotoExtractError(
            data_samples.ERROR_TEXT
        )
        self.request.path = general.AppPath.reextract.path.encode()
        coro = self.general_routes.reextract_photos(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.repo.delete_all_files.assert_called_once()
        self.repo.extract_photos.assert_called_once()
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "selected_scanner": None,
                "alerts": [
                    page_params.Alert(
                        type=page_params.AlertType.danger,
                        content=data_samples.ERROR_TEXT,
                    ).model_dump()
                ],
            }
        )

    @inlineCallbacks
    def test_save(self):
        self.add_form_content(data_samples.SAVE_REQUEST_FORM.encode())
        self.request.path = general.AppPath.save.path.encode()
        coro = self.general_routes.save(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.repo.save_file_by_index.assert_called_with(**data_samples.SAVE_REQUEST)
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "alerts": [
                    page_params.Alert(
                        type=page_params.AlertType.success,
                        content=SAVED_TO.format(dir=data_samples.OUTPUT_DIR_LINUX),
                    ).model_dump()
                ],
            }
        )

    @inlineCallbacks
    def test_save_all(self):
        self.add_form_content(data_samples.SAVE_ALL_REQUEST_FORM.encode())
        self.request.path = general.AppPath.save.path.encode()
        coro = self.general_routes.save(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.repo.save_all_files.assert_called_with(**data_samples.SAVE_ALL_REQUEST)
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "alerts": [
                    page_params.Alert(
                        type=page_params.AlertType.success,
                        content=SAVED_TO.format(dir=data_samples.OUTPUT_DIR_LINUX),
                    ).model_dump()
                ],
            }
        )

    @inlineCallbacks
    def test_delete_picture(self):
        self.add_form_content(data_samples.DELETE_REQUEST_FORM.encode())
        self.request.path = general.AppPath.delete.path.encode()
        coro = self.general_routes.delete_picture(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.repo.delete_file.assert_called_with(data_samples.DELETE_INDEX)
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "alerts": [DELETED_ALERT.model_dump()],
            }
        )

    def test_handle_default_error(self):
        failure = mock.MagicMock()
        self.general_routes.handle_default_error(self.request, failure)
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "scanners": [],
                "image_list": None,
                "image_index": None,
                "alerts": [BACKEND_ERROR_ALERT.model_dump()],
                "temp_file_exists": False,
            }
        )

    @inlineCallbacks
    def test_reset_config(self):
        coro = self.general_routes.reset_config(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.repo.reset_settings.assert_called_with()
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "alerts": [DEFAULT_SETTINGS_RESTORED_ALERT.model_dump()],
            }
        )

    @inlineCallbacks
    def test_update_extract_config(self):
        self.add_form_content(data_samples.SCAN_REQUEST_FORM.encode())
        self.request.path = general.ExtractConfigPath.update.path.encode()
        coro = self.general_routes.update_extract_config(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.assertEqual(
            data_samples.SCANNER_DEVICE_NAME, self.general_routes.selected_scanner
        )
        self.repo.update_extract_config.assert_called_with(
            scan_request=request_models.ScanRequest(**data_samples.SCAN_REQUEST)
        )
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "selected_scanner": data_samples.SCANNER_DEVICE_NAME,
            }
        )

    @inlineCallbacks
    def test_update_accordion(self):
        self.add_form_content(data_samples.UPDATE_ACCORDION_FORM.encode())
        self.request.path = general.AppPath.accordion.path.encode()
        coro = self.general_routes.update_accordion(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.assertTrue(self.general_routes.config_accordion)
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "accordion": True,
            }
        )

    @inlineCallbacks
    def test_update_save_config(self):
        self.add_form_content(data_samples.UPDATE_SAVE_CONFIG_FORM.encode())
        self.request.path = general.SaveConfigPath.update.path.encode()
        coro = self.general_routes.update_save_config(self.request)
        yield defer.Deferred.fromCoroutine(coro)
        self.assertEqual(
            self.general_routes.filename_prefix, data_samples.CUSTOM_FILENAME_PREFIX
        )
        self.page.render.assert_called_with(
            **{
                **data_samples.PAGE_RENDER_PARAMS,
                "gui_filename_prefix": data_samples.CUSTOM_FILENAME_PREFIX,
            }
        )

    def test_close(self):
        self.general_routes.close(self.request)
        self.repo.close.assert_called_once()

    def test_redirect_to_main(self):
        self.general_routes.redirect_to_main(self.request)
        self.redirect.assert_called_once()

    def test_get_js(self):
        self.general_routes.get_js(self.request)
        self.file.assert_called_once()

    def test_get_css(self):
        self.general_routes.get_css(self.request)
        self.file.assert_called_once()

    def test_get_images(self):
        self.general_routes.get_images(self.request)
        self.file.assert_called_once()

    def test_get_temp(self):
        self.general_routes.get_temp(self.request)
        self.file.assert_called_once()
