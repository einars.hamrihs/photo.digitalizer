#!/bin/bash
source app_details.sh
INSTALL_CONFIG="$INST_CONFIG_DIR/win_setup.iss"

WIN_FAVICON_PATH=$(echo "$FAVICON_PATH" | sed 's/\//\\\\/g')
sed -i "s/^#define Name.*/#define Name \"$TITLE\"/" "$INSTALL_CONFIG"
sed -i "s/^#define PackageName.*/#define PackageName \"$PACKAGE_NAME\"/" "$INSTALL_CONFIG"
sed -i "s/^#define Version.*/#define Version \"$VERSION\"/" "$INSTALL_CONFIG"
sed -i "s/^#define Publisher.*/#define Publisher \"$MAINTAINER\"/" "$INSTALL_CONFIG"
sed -i "s/^#define ExeName.*/#define ExeName \"$TITLE.exe\"/" "$INSTALL_CONFIG"
sed -i "s/^#define SetupName.*/#define SetupName \"setup_$PACKAGE_NAME\_$VERSION\"/" "$INSTALL_CONFIG"
sed -i "s/^#define OutputDir.*/#define OutputDir \"$OUTPUT_DIR\"/" "$INSTALL_CONFIG"
sed -i "s/^#define ReadmePath.*/#define ReadmePath \"$README_PATH\"/" "$INSTALL_CONFIG"
sed -i "s/^#define LicencePath.*/#define LicencePath \"$LICENCE_PATH\"/" "$INSTALL_CONFIG"
sed -i "s/^#define FaviconPath.*/#define FaviconPath \"$WIN_FAVICON_PATH\"/" "$INSTALL_CONFIG"
