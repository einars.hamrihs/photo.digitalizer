from twisted.internet.utils import getProcessOutput
from twisted.logger import Logger

from src.models import data_models
from src.settings import Settings
from src.system_operations.scanner_operator import SCANNED_FILENAME
from src.system_operations.scanner_operator import ScannerError
from src.system_operations.scanner_operator import ScannerOperator
import os

settings = Settings()

LIST_SCANNERS_SCRIPT = f"{settings.sys_directories.scripts_dir}list_scanners.ps1"
POWERSHELL_EXEC = "{}\\System32\\WindowsPowerShell\\v1.0\\powershell.exe"
POWERSHELL_ARGS = ("-noprofile", "-executionpolicy", "bypass", "-File")
SCAN_SCRIPT = f"{settings.sys_directories.scripts_dir}scan.ps1"
CONVERTED_FORMAT = "png"
SCANNED_FILE_NAME = f"{SCANNED_FILENAME}.{CONVERTED_FORMAT}"
SCANNED_FILE_PATH = f"{settings.sys_directories.scanned_images_path}{SCANNED_FILE_NAME}"


class WindowsScanner(ScannerOperator):
    log = Logger()

    @property
    def powershell_exec(self) -> str:
        return POWERSHELL_EXEC.format(os.environ["SystemRoot"])

    async def list_scanners(self) -> list[data_models.ScannerData]:
        try:
            result: bytes = await getProcessOutput(
                self.powershell_exec, [*POWERSHELL_ARGS, LIST_SCANNERS_SCRIPT]
            )
            return [
                data_models.ScannerData.create_from_wia(scanner_name)
                for scanner_name in {
                    scanner.strip("\r") for scanner in result.decode().split("\n")
                }
                if data_models.ScannerData.wia_valid_details(scanner_name)
            ]
        except IOError as e:
            raise ScannerError(str(e))

    async def scan(self, device_name: str) -> bytes:
        self.log.info("Scanning with scanner: {scanner}", scanner=device_name)
        try:
            return await getProcessOutput(
                self.powershell_exec,
                [
                    *POWERSHELL_ARGS,
                    SCAN_SCRIPT,
                    device_name,
                    SCANNED_FILE_PATH,
                ],
            )
        except IOError as e:
            raise ScannerError(str(e))

    async def scan_and_save_temp_file(self, device_name: str) -> None:
        await self.scan(device_name)
