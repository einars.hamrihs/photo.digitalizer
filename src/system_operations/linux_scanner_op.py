from subprocess import check_output, CalledProcessError

from twisted.internet import threads
from twisted.logger import Logger
from typing import Optional

from src.models import data_models, general
from src.settings import Settings
from src.system_operations.scanner_operator import (
    SCANNED_FILENAME,
    ScannerError,
    ScannerOperator,
)
from src.utils import cv2_tools

settings = Settings()


class SnapInterfaceError(Exception):
    pass


SNAP_INTERFACE_SCRIPTS = f"{settings.sys_directories.scripts_dir}/snap_interfaces"
CHECK_SNAP_INTERFACES = f"{SNAP_INTERFACE_SCRIPTS}/check_on_start.sh"
CHECK_SNAP_INTERFACES_ON_SCANNING = f"{SNAP_INTERFACE_SCRIPTS}/check_on_scanning.sh"
SCANNED_FORMAT = "tiff"
LIST_SCANNERS_EXEC = f"{settings.sys_directories.scripts_dir}/list_scanners.sh"
SCAN_EXEC = f"{settings.sys_directories.scripts_dir}/scan.sh"
CONVERTED_FORMAT = "png"
SCANNED_FILE_NAME = f"{SCANNED_FILENAME}.{SCANNED_FORMAT}"
SCANNED_FILE_PATH = (
    f"{settings.sys_directories.scanned_images_path}/{SCANNED_FILE_NAME}"
)
CONVERTED_FILE_PATH = f"{settings.sys_directories.scanned_images_path}/{SCANNED_FILENAME}.{CONVERTED_FORMAT}"
INTERFACE_NOT_CONNECTED_ERROR = f"""Interface {{interface}} not connected.
Connect manually by running these commands in the terminal:
`snap connect pixelyse:{general.SnapInterface.avahi_observe}`
`snap connect pixelyse:{general.SnapInterface.hardware_observe}`
"""
SNAP_INTERFACE_DEFAULT_ERROR = "Snap interface error"
RAW_USB_INTERFACE_ERROR = f"""If you scanner is connected via USB, try connecting `raw-usb` snap interface (if it is not already connected) by running this command in the terminal:
`snap connect pixelyse:{general.SnapInterface.raw_usb}`
"""

SNAP_INTERFACE_ERRORS = {
    general.SnapInterface.avahi_observe.error_code: INTERFACE_NOT_CONNECTED_ERROR.format(
        interface=general.SnapInterface.avahi_observe
    ),
    general.SnapInterface.raw_usb.error_code: INTERFACE_NOT_CONNECTED_ERROR.format(
        interface=general.SnapInterface.raw_usb
    ),
    general.SnapInterface.hardware_observe.error_code: INTERFACE_NOT_CONNECTED_ERROR.format(
        interface=general.SnapInterface.hardware_observe
    ),
}


class LinuxScanner(ScannerOperator):
    log = Logger()

    @staticmethod
    async def check_snap_startup_interfaces() -> None:
        try:
            await threads.deferToThread(check_output, [CHECK_SNAP_INTERFACES])
        except CalledProcessError as e:
            raise SnapInterfaceError(
                SNAP_INTERFACE_ERRORS.get(
                    e.output.decode().strip(), SNAP_INTERFACE_DEFAULT_ERROR
                )
            )

    @staticmethod
    async def check_snap_scanning_interfaces() -> bool:
        try:
            await threads.deferToThread(
                check_output, [CHECK_SNAP_INTERFACES_ON_SCANNING]
            )
        except CalledProcessError as e:
            if e.output.decode().strip() == general.SnapInterface.raw_usb.error_code:
                return False
            raise SnapInterfaceError(
                SNAP_INTERFACE_ERRORS.get(
                    e.output.decode().strip(), SNAP_INTERFACE_DEFAULT_ERROR
                )
            )
        return True

    async def add_snap_interface_error(self) -> str:
        if settings.is_snap and not await self.check_snap_scanning_interfaces():
            return f"""
            {RAW_USB_INTERFACE_ERROR}
            """
        return ""

    async def list_scanners(self) -> list[data_models.ScannerData]:
        if settings.is_snap:
            await self.check_snap_startup_interfaces()
        try:
            result: bytes = await threads.deferToThread(
                check_output, [LIST_SCANNERS_EXEC]
            )
            return [
                data_models.ScannerData.create_from_scanimage(scanner)
                for scanner in result.decode().split(";")
                if data_models.ScannerData.scanimage_valid_details(scanner)
            ]
        except CalledProcessError as e:
            error = str(e.output.decode())
            error += await self.add_snap_interface_error()
            raise ScannerError(error)

    async def scan(self, device_name: str) -> bytes:
        self.log.info("Scanning with scanner: {scanner}", scanner=device_name)
        try:
            return await threads.deferToThread(
                check_output,
                [SCAN_EXEC, device_name, SCANNED_FORMAT, SCANNED_FILE_PATH],
            )
        except CalledProcessError as e:
            error = str(e.output.decode())
            error += await self.add_snap_interface_error()
            raise ScannerError(error)

    @staticmethod
    async def convert_to_png() -> None:
        img_array = cv2_tools.create_image_array(SCANNED_FILE_PATH)
        cv2_tools.write_image_file(img=img_array, path=CONVERTED_FILE_PATH)

    async def scan_and_save_temp_file(self, device_name: str) -> str:
        await self.scan(device_name)
        await self.convert_to_png()
        return SCANNED_FILE_NAME
