import platform

from src.models import general


def is_windows() -> bool:
    return platform.system() == general.System.Windows
