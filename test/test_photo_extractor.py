from unittest import mock

from numpy.testing import assert_array_equal
from twisted.trial import unittest

from src.photo_extractor.photo_extractor import PhotoExtractor, PhotoExtractError
from test.patchers.settings import patch_settings
from test.samples import data_samples
from test.samples import picture_samples
from test.patchers.time import patch_time


MODULE_PATH = "src.photo_extractor.photo_extractor"


class TestPhotoExtractor(unittest.TestCase):
    def setUp(self):
        patch_time([MODULE_PATH])
        self.patch_imwrite()
        self.photo_extractor = PhotoExtractor(
            mock.MagicMock(**data_samples.PHOTO_EXTRACT_PARAMS)
        )

    def patch_imwrite(self):
        self.imwrite = mock.MagicMock()
        patcher = mock.patch("src.utils.cv2_tools.cv2.imwrite", self.imwrite)
        patcher.start()

    @staticmethod
    def set_image_path(image_path: str):
        patch_settings(
            locations=[MODULE_PATH],
            system_directories={
                **data_samples.SYS_DIRECTORIES,
                "scanned_images_path": image_path,
            },
        )

    @staticmethod
    def get_written_content(call: mock.call):
        return call[0][1]

    def test_extract_photos(self):
        self.set_image_path(data_samples.PHOTO_1_PATH)
        self.photo_extractor.extract_photos()
        self.imwrite.assert_has_calls(
            [
                mock.call(picture_samples.EX1_OUTPUT_1_PATH, mock.ANY),
                mock.call(picture_samples.EX1_OUTPUT_2_PATH, mock.ANY),
            ]
        )
        assert_array_equal(
            self.get_written_content(self.imwrite.call_args_list[0]),
            picture_samples.EX1_OUTPUT_1_CONTENT,
        )
        assert_array_equal(
            self.get_written_content(self.imwrite.call_args_list[1]),
            picture_samples.EX1_OUTPUT_2_CONTENT,
        )

    def test_extract_photos_no_photos_found(self):
        self.set_image_path(data_samples.PHOTO_2_PATH)
        with self.assertRaises(PhotoExtractError):
            self.photo_extractor.extract_photos()
            self.imwrite.assert_not_called()
