const backgroundThresholdInput = document.getElementById('backgroundThreshholdInput');
const backgroundThresholdValue = document.getElementById('backgroundThreshholdVal');
const minAspectRatioInput = document.getElementById('minAspectRatioInput');
const minAspectRatioValue = document.getElementById('minAspectRatioVal');
const maxAspectRatioInput = document.getElementById('maxAspectRatioInput');
const maxAspectRatioValue = document.getElementById('maxAspectRatioVal');
const minPhotoAreaInput = document.getElementById('minPhotoAreaInput');
const minPhotoAreaValue = document.getElementById('minPhotoAreaVal');
const erodeIterationsInput = document.getElementById('erodeIterationsInput');
const erodeIterationsValue = document.getElementById('erodeIterationsVal');
const scannerList = document.getElementById('scannerList');
const scanBtn = document.getElementById('scanBtn');
const scanForm = document.getElementById('scanForm')
const reExtractBtn= document.getElementById('reExtractBtn')
const scanningProgressDiv = document.getElementById('scanningProgress')
const extractingProgress = document.getElementById('extractingProgress')
const imageViewDiv = document.getElementById('imageView')
const configAccordion = document.getElementById('collapseAdvanced');
const filenamePrefixInput = document.getElementById('filenamePrefixInput');
const saveForm = document.getElementById('saveForm');
const alertElements = document.querySelectorAll('.alert');

function updateBackgroundThresholdSpan(){
    backgroundThresholdValue.textContent = backgroundThresholdInput?.value;
}

function updateMinAspectRatioSpan() {
    minAspectRatioValue.textContent = minAspectRatioInput?.value;
}

function updateMaxAspectRatioSpan() {
    maxAspectRatioValue.textContent = maxAspectRatioInput?.value;
}

function updateMinPhotoAreaSpan() {
    minPhotoAreaValue.textContent = minPhotoAreaInput?.value;
}

function updateErodeIterationsSpan() {
    erodeIterationsValue.textContent = erodeIterationsInput?.value;
}

function updateButtonState() {
    if (scannerList.value === 'chooseScanner') {
        scanBtn.disabled = true;
    } else {
        scanBtn.disabled = false;
    }
}

function changeConfig() {
    scanForm.action = "/extract_config/update";
    scanForm.submit();
}

function changeSaveConfig() {
    saveForm.action = "/save_config/update";
    saveForm.submit();
}

function scanInProgress() {
    scanForm.hidden = true;
    scanningProgressDiv.hidden = false;
    imageViewDiv.hidden = true;
    alertElements.forEach(element => {
        element.setAttribute('hidden', true);
    });
}

function extractingInProgress() {
    scanForm.hidden = true;
    extractingProgress.hidden = false;
    imageViewDiv.hidden = true;
}

function configAccordionChanged(event) {
    let form = document.createElement("form");
    let status = document.createElement("input");

    form.method = "POST";
    form.action = "/accordion";
    status.value = event.type;
    status.name="status";
    form.appendChild(status);
    document.body.appendChild(form);
    form.submit();
}

backgroundThresholdInput?.addEventListener('input', updateBackgroundThresholdSpan);
minAspectRatioInput?.addEventListener('input', updateMinAspectRatioSpan);
maxAspectRatioInput?.addEventListener('input', updateMaxAspectRatioSpan);
minPhotoAreaInput?.addEventListener('input', updateMinPhotoAreaSpan);
erodeIterationsInput?.addEventListener('input', updateErodeIterationsSpan);
scannerList?.addEventListener('change', updateButtonState);
filenamePrefixInput?.addEventListener('change', changeSaveConfig);
scanForm?.addEventListener('change', changeConfig);
scanBtn?.addEventListener('click', scanInProgress);
reExtractBtn?.addEventListener('click', extractingInProgress)
configAccordion?.addEventListener('shown.bs.collapse', configAccordionChanged);
configAccordion?.addEventListener('hidden.bs.collapse', configAccordionChanged);

updateButtonState();
updateBackgroundThresholdSpan();
updateMinAspectRatioSpan();
updateMaxAspectRatioSpan();
updateMinPhotoAreaSpan();
updateErodeIterationsSpan();
