from __future__ import annotations

from typing import Optional

from twisted.logger import Logger
from twisted.python.failure import Failure
from twisted.web.server import Request
from twisted.web.static import File
from twisted.web.util import Redirect

from src.models import general
from src.models import page_params
from src.models import request_models
from src.photo_extractor.photo_extractor import PhotoExtractError
from src.settings import Settings
from src.system_operations.linux_scanner_op import (
    SnapInterfaceError,
    RAW_USB_INTERFACE_ERROR,
)
from src.system_operations.scanner_operator import ScannerError
from src.utils.web_app_utils import format_request
from src.web_app.routes.default import RoutesBase

settings = Settings()


NO_SCANNERS_FOUND = "No scanners found"
NO_SCANNERS_FOUND_SNAP = f"""{NO_SCANNERS_FOUND}.
{RAW_USB_INTERFACE_ERROR}
"""
LOADING_SCANNERS = "Loading scanners"
DELETED = "Deleted successfully"
SAVED_TO = "Saved to {dir}"
DELETED_ALERT = page_params.Alert(type=page_params.AlertType.success, content=DELETED)
BACKEND_ERROR = "Backend error. Please restart the application or refresh the scanners."
BACKEND_ERROR_ALERT = page_params.Alert(
    type=page_params.AlertType.danger, content=BACKEND_ERROR
)
DEFAULT_SETTINGS_RESTORED = "Default settings restored"
DEFAULT_SETTINGS_RESTORED_ALERT = page_params.Alert(
    type=page_params.AlertType.success, content=DEFAULT_SETTINGS_RESTORED
)


class GeneralRoutes(RoutesBase):
    log = Logger()

    def __init__(self, *args, **kwargs):
        self.selected_scanner = None
        self.config_accordion = False
        self.image_index = 0
        self.filename_prefix = None
        super().__init__(*args, **kwargs)

    @staticmethod
    def get_js(request: Request) -> File:
        return File(settings.static_path + general.AppPath.js.full_path)

    @staticmethod
    def get_css(request: Request) -> File:
        return File(settings.static_path + general.AppPath.css.full_path)

    @staticmethod
    def get_images(request: Request) -> File:
        return File(settings.static_path + general.AppPath.images.full_path)

    @staticmethod
    def get_temp(request: Request) -> File:
        return File(settings.sys_directories.scanned_images_path)

    @staticmethod
    def redirect_to_main(request: Request) -> Redirect:
        return Redirect(general.AppPath.main.path.encode())

    def handle_default_error(self, request: Request, failure: Failure) -> str:
        self.log.debug(str(failure.getErrorMessage()))
        return self.render_template(
            path=general.AppPath.main,
            params=page_params.MainParams(
                scanners=[],
                advanced_params=self.repo.config_serialized,
                alerts=[BACKEND_ERROR_ALERT],
                scanners_updated=self.repo.scanners_updated,
            ),
        )

    async def get_scanners(
        self,
        request_data: request_models.MainRequest,
        alerts: Optional[list[page_params.Alert]],
    ) -> Optional[list[dict]]:
        try:
            return await self.repo.list_scanners_serialized(
                reload=request_data.reload_scanners, reset=request_data.reset_scanners
            )
        except SnapInterfaceError as e:
            alerts.append(
                page_params.Alert(type=page_params.AlertType.danger, content=str(e))
            )

    async def create_no_scanners_found_error(self) -> page_params.Alert:
        alert_content = NO_SCANNERS_FOUND
        if settings.is_snap and not await self.repo.check_snap_scanning_interfaces():
            alert_content = NO_SCANNERS_FOUND_SNAP
        return page_params.Alert(
            type=page_params.AlertType.danger, content=alert_content
        )

    async def render_main(
        self,
        request: Request,
        request_data: request_models.MainRequest = request_models.MainRequest(),
        alerts: Optional[list[page_params.Alert]] = None,
    ) -> str:
        scanners = await self.get_scanners(request_data=request_data, alerts=alerts)
        if self.repo.no_scanners_found:
            alerts.append(
                await self.create_no_scanners_found_error(),
            )
        if not self.repo.scanner_exists(self.selected_scanner):
            self.selected_scanner = None
        return self.render_template(
            path=general.AppPath.main,
            params=page_params.MainParams(
                advanced_params=self.repo.config_serialized,
                scanners=scanners,
                image_list=self.repo.get_image_file_list(),
                image_index=self.image_index,
                alerts=alerts,
                selected_scanner=self.selected_scanner,
                accordion=self.config_accordion,
                temp_file_exists=self.repo.temp_file_exists,
                gui_filename_prefix=self.filename_prefix,
                scanners_updated=self.repo.scanners_updated,
            ),
        )

    @format_request()
    async def main(
        self,
        request: Request,
        alerts: list,
        request_data: request_models.MainRequest = request_models.MainRequest(),
    ) -> str:
        self.image_index = request_data.image_index
        return await self.render_main(
            request=request, alerts=alerts, request_data=request_data
        )

    @format_request()
    async def scan(
        self, request: Request, alerts: list, request_data: request_models.ScanRequest
    ) -> str:
        self.log.debug("new scan request: {data}", data=request_data)
        self.selected_scanner = request_data.device_name
        self.repo.delete_all_files()
        try:
            await self.repo.scan_and_extract_photos(request_data)
        except (ScannerError, PhotoExtractError) as e:
            alerts.append(
                page_params.Alert(type=page_params.AlertType.danger, content=str(e))
            ),
            return await self.render_main(
                request=request,
                alerts=alerts,
            )
        self.image_index = 0
        return await self.render_main(request=request, alerts=alerts)

    async def reextract_photos(self, request: Request) -> str:
        self.log.debug("re-extract request")
        self.repo.delete_all_files()
        try:
            self.repo.extract_photos()
        except PhotoExtractError as e:
            return await self.render_main(
                request=request,
                alerts=[
                    page_params.Alert(type=page_params.AlertType.danger, content=str(e))
                ],
            )
        self.image_index = 0
        return await self.render_main(request=request, alerts=[])

    @format_request()
    async def save(
        self, request: Request, alerts: list, request_data: request_models.SaveRequest
    ) -> str:
        self.log.debug("save request: {data}", data=request_data)
        if request_data.saved_index is None:
            return await self.save_all(request=request, request_data=request_data)
        self.repo.save_file_by_index(
            saved_index=request_data.saved_index,
            filename_prefix=request_data.filename_prefix,
        )
        alerts.append(
            page_params.Alert(
                type=page_params.AlertType.success,
                content=SAVED_TO.format(dir=settings.sys_directories.output_dir),
            )
        )
        return await self.render_main(
            request=request,
            request_data=request_data,
            alerts=alerts,
        )

    async def save_all(
        self, request: Request, request_data: request_models.SaveRequest
    ) -> str:
        self.log.debug("save all request: {data}", data=request_data)
        self.repo.save_all_files(filename_prefix=request_data.filename_prefix)
        return await self.render_main(
            request=request,
            request_data=request_data,
            alerts=[
                page_params.Alert(
                    type=page_params.AlertType.success,
                    content=SAVED_TO.format(dir=settings.sys_directories.output_dir),
                )
            ],
        )

    @format_request()
    async def delete_picture(
        self,
        request: Request,
        alerts: list,
        request_data: request_models.MainPostRequest,
    ) -> str:
        self.log.debug("delete picture request: {data}", data=request_data)
        self.image_index = self.repo.delete_file(request_data.image_index)
        alerts.append(DELETED_ALERT)
        return await self.render_main(
            request=request,
            request_data=request_data,
            alerts=alerts,
        )

    async def reset_config(self, request: Request) -> str:
        self.log.debug("reset config request")
        self.repo.reset_settings()
        return await self.render_main(
            request=request,
            alerts=[DEFAULT_SETTINGS_RESTORED_ALERT],
        )

    @format_request()
    async def update_extract_config(
        self, request: Request, alerts: list, request_data: request_models.ScanRequest
    ) -> str:
        self.log.debug("update extract config request")
        self.selected_scanner = request_data.device_name
        self.repo.update_extract_config(scan_request=request_data)
        return await self.render_main(request=request, alerts=alerts)

    @format_request()
    async def update_accordion(
        self,
        request: Request,
        alerts: list,
        request_data: request_models.UpdateAccordionRequest,
    ) -> str:
        self.log.debug("update config request: {data}", data=request_data)
        self.config_accordion = request_data.status
        return await self.render_main(request=request, alerts=alerts)

    @format_request()
    async def update_save_config(
        self, request: Request, alerts: list, request_data: request_models.SaveRequest
    ) -> str:
        self.log.debug("update save config request: {data}", data=request_data)
        self.filename_prefix = request_data.gui_filename_prefix
        return await self.render_main(request=request, alerts=alerts)

    def close(self, request: Request) -> None:
        self.repo.close()
