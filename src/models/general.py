from enum import StrEnum

from src.models.base_types import Path


class RESTMethod(StrEnum):
    GET = "GET"
    POST = "POST"
    DELETE = "DELETE"
    PUT = "PUT"


class AppPath(Path):
    _object_type = ""
    root = ""
    js = "js"
    css = "css"
    images = "images"
    main = "main"
    scan = "scan"
    save = "save"
    close = "close"
    delete = "delete"
    temp = "temp"
    accordion = "accordion"
    reextract = "reextract"


class ExtractConfigPath(Path):
    _object_type = "extract_config"
    update = "update"
    reset = "reset"


class SaveConfigPath(Path):
    _object_type = "save_config"
    update = "update"


class ModelDumpMode(StrEnum):
    json = "json"
    python = "python"


class ContentType(StrEnum):
    json = "application/json"
    x_www_form_urlencoded = "application/x-www-form-urlencoded"


class Encoding(StrEnum):
    utf_8 = "utf-8"


class System(StrEnum):
    Linux = "Linux"
    Windows = "Windows"


class SnapInterface(StrEnum):
    avahi_observe = "avahi-observe"
    raw_usb = "raw-usb"
    hardware_observe = "hardware-observe"

    @property
    def error_code(self):
        return f"{self.value}_not_connected"
