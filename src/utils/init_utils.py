from src.system_operations.file_operator import FileOperator
from src.settings import Settings

settings = Settings()


def create_temp_dir():
    file_op = FileOperator()
    file_op.create_directory(settings.sys_directories.temp_dir)
    return settings.sys_directories.temp_dir
