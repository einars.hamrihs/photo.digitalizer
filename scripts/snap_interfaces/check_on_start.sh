#!/bin/bash

interfaces=("avahi-observe" "hardware-observe")

SCRIPTDIR=`dirname $0`

$SCRIPTDIR/check_interfaces.sh "${interfaces[@]}"
