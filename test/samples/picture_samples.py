import cv2

from test.samples import data_samples


EX1_OUTPUT_1_PATH = f"{data_samples.PHOTO_1_PATH}{data_samples.TEMP_FILENAME1}"
EX1_OUTPUT_1_CONTENT = cv2.imread(EX1_OUTPUT_1_PATH, cv2.IMREAD_UNCHANGED)
EX1_OUTPUT_2_PATH = f"{data_samples.PHOTO_1_PATH}{data_samples.TEMP_FILENAME2}"
EX1_OUTPUT_2_CONTENT = cv2.imread(EX1_OUTPUT_2_PATH, cv2.IMREAD_UNCHANGED)
