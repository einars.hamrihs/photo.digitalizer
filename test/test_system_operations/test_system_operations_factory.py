from twisted.trial import unittest

from src.system_operations.factory import scanner_operator_factory
from src.system_operations.scanner_operator import ScannerOperator
from test.patchers.settings import patch_settings

MODULE_PATH = "src.system_operations.factory"


class TestSystemOperationsFactory(unittest.TestCase):
    def setUp(self):
        patch_settings([MODULE_PATH])

    def test_scanner_operator_factory(self):
        result = scanner_operator_factory()
        self.assertIsInstance(result, ScannerOperator)
