from __future__ import annotations

from typing import Optional

import jinja2
from twisted.logger import Logger

from src.models import general
from src.models import page_params
from src.repository import Repository
from src.settings import Settings

settings = Settings()


class RoutesBase:
    log = Logger()

    def __init__(
        self,
        repo: Repository,
    ):
        self.templates = jinja2.Environment(
            loader=jinja2.FileSystemLoader(settings.templates_path)
        )
        self.repo = repo

    def render_template(
        self,
        path: general.AppPath,
        params: Optional[page_params.PageParams] = None,
    ) -> str:
        page = self.templates.get_template(path.html)
        params = params.model_dump(mode=general.ModelDumpMode.python) if params else {}
        return page.render(title=settings.app_name, **params)
