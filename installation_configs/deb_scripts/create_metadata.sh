#!/bin/bash
source app_details.sh

# Create metadata
cat <<EOF > $PACKAGE_DIR/DEBIAN/$PACKAGE_NAME.appdata.xml
<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <id>$DEB_PRODUCT_ID</id>
  <name>$TITLE</name>
  <summary>$SUMMARY</summary>
  <description>
    <p>$DESCRIPTION</p>
  </description>
  <content_rating>none</content_rating>
  <metadata_license>FSFAP</metadata_license>
  <project_license>MIT</project_license>
  <icon type="remote">$ICON_URL_64</icon>
  <url type="homepage">$HOMEPAGE</url>
  <url type="help">$HELP_URL</url>
  <url type="contact">$CONTACT_URL</url>
  <url type="donation">$DONATION_URL</url>
  <developer_name>$MAINTAINER</developer_name>
  <categories>
    <category>Utility</category>
  </categories>
  <screenshots>
  <screenshot type="default">
    <caption>$TITLE advanced settings</caption>
    <image type="source">$SCREENSHOT_URL_1</image>
  </screenshot>
  <screenshot>
    <caption>$TITLE extracted photos</caption>
    <image type="source">$SCREENSHOT_URL_2</image>
  </screenshot>
 </screenshots>
</component>
EOF
