#!/bin/bash

check_interface() {
    local interface=$1
    if ! snapctl is-connected "$interface"; then
        echo "${interface}_not_connected"
        exit 1
    fi
}

for interface in "$@"; do
    check_interface "$interface"
done

exit 0
