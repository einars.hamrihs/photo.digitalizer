from src.models import general
from src.system_operations.linux_scanner_op import LinuxScanner
from src.system_operations.win_scanner_op import WindowsScanner
from src.system_operations.scanner_operator import ScannerOperator
from src.settings import Settings


settings = Settings()


def scanner_operator_factory() -> ScannerOperator:
    return {
        general.System.Linux: LinuxScanner,
        general.System.Windows: WindowsScanner,
    }.get(settings.system)()
