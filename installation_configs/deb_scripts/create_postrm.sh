#!/bin/bash
source app_details.sh

# Create the pre-removal script
cat <<EOF > $PACKAGE_DIR/DEBIAN/postrm
#!/bin/bash
rm -rf $INSTALL_DIR
EOF
chmod 755 $PACKAGE_DIR/DEBIAN/postrm
