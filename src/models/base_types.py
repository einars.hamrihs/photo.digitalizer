from enum import Enum


class Path(Enum):
    @property
    def object_type(self):
        obj_type = getattr(self, "_object_type").value
        return f"/{obj_type}" if obj_type else ""

    @property
    def path(self):
        return f"{self.object_type}/{self.value}"

    @property
    def full_path(self):
        return f"{self.object_type}/{self.name}/"

    @property
    def html(self):
        return f"{self.object_type}/{self.name}.html"
