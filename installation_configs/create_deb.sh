#!/bin/bash
source app_details.sh
SCRIPTS_DIR="./installation_configs/deb_scripts"

echo "Creating temporary directories."
$SCRIPTS_DIR/makedirs.sh
echo "Copying files to the temporary directories."
$SCRIPTS_DIR/copy_files.sh
echo "Creating the control file."
$SCRIPTS_DIR/create_control.sh
echo "Creating the metadata."
$SCRIPTS_DIR/create_metadata.sh
echo "Creating the postinst file."
$SCRIPTS_DIR/create_postinst.sh
echo "Creating the postrm file."
$SCRIPTS_DIR/create_postrm.sh
echo "Creating the desktop file."
$SCRIPTS_DIR/create_desktop.sh

echo "Building the deb package."
dpkg-deb --build $PACKAGE_DIR

echo "Cleaning up."
rm -rf $PACKAGE_DIR

echo "The package $PACKAGE_DIR.deb has been created successfully."
