from typing import Any, Optional

from pydantic import BaseModel

PHOTO_FILENAME_PREFIX = "img"
ACCORDION_SHOWN = "shown.bs.collapse"
ACCORDION_HIDDEN = "hidden.bs.collapse"


def get_single_param(request_data: dict, key: str) -> Any:
    if request_data.get(key):
        return request_data.get(key)[0]


def create_single_param_dict(request_data: dict) -> dict:
    output = {}
    for key, val in request_data.items():
        if val is not None and val != "":
            output[key] = get_single_param(request_data=request_data, key=key)
    return output


class MainRequest(BaseModel):
    image_index: Optional[int] = 0
    reload_scanners: Optional[bool] = False
    reset_scanners: Optional[bool] = False

    @classmethod
    def create(cls, request_data: dict) -> "MainRequest":
        return cls(
            **request_data,
        )


class ScanRequest(BaseModel):
    device_name: str
    background_threshold: int
    min_aspect_ratio: float
    max_aspect_ratio: float
    min_photo_area: int
    erode_iterations: int

    @classmethod
    def create(cls, request_data: dict) -> "ScanRequest":
        return cls(**create_single_param_dict(request_data))


class MainPostRequest(MainRequest):
    @classmethod
    def create(cls, request_data: dict) -> "MainPostRequest":
        return cls(
            image_index=get_single_param(request_data=request_data, key="image_index"),
        )


class SaveRequest(MainPostRequest):
    filename_prefix: Optional[str] = PHOTO_FILENAME_PREFIX
    saved_index: Optional[int] = None

    @classmethod
    def create(cls, request_data: dict) -> "MainPostRequest":
        return cls(**create_single_param_dict(request_data))

    @property
    def gui_filename_prefix(self) -> str:
        return (
            self.filename_prefix
            if self.filename_prefix != PHOTO_FILENAME_PREFIX
            else None
        )


class UpdateAccordionRequest(MainPostRequest):
    status: bool

    @classmethod
    def create(cls, request_data: dict) -> "UpdateAccordionRequest":
        status_map = {ACCORDION_SHOWN: True, ACCORDION_HIDDEN: False}
        return cls(status=status_map[create_single_param_dict(request_data)["status"]])
