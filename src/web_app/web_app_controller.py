from typing import Optional

import jinja2
from klein import Klein
from twisted.internet import defer
from twisted.logger import Logger

from src.models import general
from src.repository import Repository
from src.settings import Settings
from src.web_app.routes.general import GeneralRoutes

settings = Settings()


class WebAppController:
    log = Logger()

    def __init__(self, app: Klein, app_deferred: defer.Deferred):
        self.app = app
        self.general_routes: Optional[GeneralRoutes] = None
        self.templates = jinja2.Environment(
            loader=jinja2.FileSystemLoader(settings.templates_path)
        )
        self.app_deferred = app_deferred

    def start(self, repo: Repository):
        self.set_up_route_controllers(repo=repo)
        self.register_routes()

    def set_up_route_controllers(self, repo: Repository):
        self.general_routes = GeneralRoutes(
            repo=repo,
        )

    def register_routes(self) -> None:
        self.app.route(general.AppPath.root.path, methods=[general.RESTMethod.GET])(
            self.general_routes.redirect_to_main
        )
        self.app.route(
            general.AppPath.js.path, methods=[general.RESTMethod.GET], branch=True
        )(self.general_routes.get_js)
        self.app.route(
            general.AppPath.css.path, methods=[general.RESTMethod.GET], branch=True
        )(self.general_routes.get_css)
        self.app.route(
            general.AppPath.images.path, methods=[general.RESTMethod.GET], branch=True
        )(self.general_routes.get_images)
        self.app.route(
            general.AppPath.temp.path, methods=[general.RESTMethod.GET], branch=True
        )(self.general_routes.get_temp)
        self.app.route(
            general.AppPath.main.path,
            methods=[general.RESTMethod.GET],
            branch=True,
        )(self.general_routes.main)
        self.app.route(
            general.AppPath.scan.path,
            methods=[general.RESTMethod.POST],
            branch=True,
        )(self.general_routes.scan)
        self.app.route(
            general.AppPath.reextract.path,
            methods=[general.RESTMethod.POST],
            branch=True,
        )(self.general_routes.reextract_photos)
        self.app.route(
            general.AppPath.save.path,
            methods=[general.RESTMethod.POST],
            branch=True,
        )(self.general_routes.save)
        self.app.route(
            general.AppPath.delete.path,
            methods=[general.RESTMethod.POST],
            branch=True,
        )(self.general_routes.delete_picture)
        self.app.route(
            general.ExtractConfigPath.reset.path,
            methods=[general.RESTMethod.POST],
            branch=True,
        )(self.general_routes.reset_config)
        self.app.route(
            general.ExtractConfigPath.update.path,
            methods=[general.RESTMethod.POST],
            branch=True,
        )(self.general_routes.update_extract_config)
        self.app.route(
            general.SaveConfigPath.update.path,
            methods=[general.RESTMethod.POST],
            branch=True,
        )(self.general_routes.update_save_config)
        self.app.route(
            general.AppPath.accordion.path,
            methods=[general.RESTMethod.POST],
            branch=True,
        )(self.general_routes.update_accordion)
        self.app.route(
            general.AppPath.close.path,
            methods=[general.RESTMethod.GET],
            branch=True,
        )(self.general_routes.close)
        self.app.handle_errors(Exception)(self.general_routes.handle_default_error)
