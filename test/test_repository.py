from unittest import mock

from twisted.internet import defer
from twisted.trial import unittest

from src.models import request_models
from src.repository import Repository
from src.system_operations.linux_scanner_op import SnapInterfaceError
from src.system_operations.scanner_operator import SCANNED_FILENAME, SCANNED_FORMAT
from test.patchers.settings import patch_settings
from test.samples import data_samples

MODULE_PATH = "src.repository"


class TestRepository(unittest.TestCase):
    def setUp(self):
        patch_settings([MODULE_PATH])
        self.app_deferred = mock.MagicMock()
        self.patch_file_operator()
        self.patch_scanner_operator()
        self.patch_photo_extractor()
        self.patch_config()
        self.repo = Repository(
            app_deferred=self.app_deferred,
            scanner_operator=self.scanner_operator,
        )

    def patch_file_operator(self):
        self.file_operator = mock.MagicMock()
        patcher = mock.patch(
            MODULE_PATH + ".FileOperator", return_value=self.file_operator
        )
        patcher.start()

    def patch_scanner_operator(self):
        self.scanner_operator = mock.AsyncMock()
        self.scanner_operator.list_scanners.return_value = [
            data_samples.SCANNER_MODEL_LINUX
        ]
        self.scanner_operator.scan_and_save_temp_file.return_value = (
            data_samples.TEMP_FILENAME1
        )
        self.scanner_operator.check_snap_scanning_interfaces.return_value = True

    def patch_photo_extractor(self):
        self.photo_extractor = mock.MagicMock()
        patcher = mock.patch(MODULE_PATH + ".PhotoExtractor", self.photo_extractor)
        patcher.start()

    def patch_config(self):
        self.config = mock.MagicMock()
        patcher = mock.patch(MODULE_PATH + ".Config", return_value=self.config)
        patcher.start()

    def test_start(self):
        self.repo.start()
        self.file_operator.create_directory.assert_has_calls(
            [
                mock.call(data_samples.SCANNED_IMAGES_PATH),
                mock.call(data_samples.OUTPUT_DIR_LINUX),
            ]
        )

    def test_config_serialized(self):
        result = self.repo.config_serialized
        self.assertEqual(result, self.config.model_dump.return_value)

    @defer.inlineCallbacks
    def test_list_scanners_serialized(self):
        self.repo.scanners = [data_samples.SCANNER_MODEL_LINUX]
        coro = self.repo.list_scanners_serialized()
        result = yield defer.Deferred.fromCoroutine(coro)
        self.assertEqual(result, [data_samples.SCANNER_DATA_LINUX])

    @defer.inlineCallbacks
    def test_list_scanners_serialized_reset(self):
        self.repo.scanners = [data_samples.SCANNER_MODEL_LINUX]
        coro = self.repo.list_scanners_serialized(reset=True)
        result = yield defer.Deferred.fromCoroutine(coro)
        self.assertIsNone(result)

    @defer.inlineCallbacks
    def test_list_scanners_serialized_reload(self):
        self.repo.scanners = []
        coro = self.repo.list_scanners_serialized(reload=True)
        result = yield defer.Deferred.fromCoroutine(coro)
        self.assertEqual(result, [data_samples.SCANNER_DATA_LINUX])

    @defer.inlineCallbacks
    def test_list_scanners_serialized_snap_interface_error(self):
        self.scanner_operator.list_scanners.side_effect = SnapInterfaceError(
            data_samples.ERROR_TEXT
        )
        with self.assertRaises(SnapInterfaceError):
            coro = self.repo.list_scanners_serialized(reload=True)
            yield defer.Deferred.fromCoroutine(coro)
            self.assertTrue(self.repo.scanners_updated)

    def test_no_scanners_found(self):
        self.repo.scanners = []
        result = self.repo.no_scanners_found
        self.assertTrue(result)

    def test_scanner_exists(self):
        self.repo.scanners = [data_samples.SCANNER_MODEL_LINUX]
        result = self.repo.scanner_exists(data_samples.SCANNER_DEVICE_NAME)
        self.assertTrue(result)

    @defer.inlineCallbacks
    def test_scan_and_extract_photos(self):
        coro = self.repo.scan_and_extract_photos(
            request_models.ScanRequest(
                device_name=data_samples.SCANNER_DEVICE_NAME,
                **data_samples.PHOTO_EXTRACT_PARAMS,
            )
        )
        yield defer.Deferred.fromCoroutine(coro)
        self.config.update_config.assert_called_once_with(
            **data_samples.PHOTO_EXTRACT_PARAMS
        )
        self.scanner_operator.scan_and_save_temp_file.assert_called_once_with(
            device_name=data_samples.SCANNER_DEVICE_NAME
        )
        self.file_operator.delete_file.assert_has_calls(
            [
                mock.call(f"{SCANNED_FILENAME}.{SCANNED_FORMAT}"),
                mock.call(data_samples.TEMP_FILENAME1),
            ]
        )
        self.photo_extractor.assert_called_once_with(self.config)
        self.photo_extractor.return_value.extract_photos.assert_called_once()

    def test_save_file_by_index(self):
        self.file_operator.list_files.side_effect = [
            data_samples.LISTDIR_RESPONSE_TEMP,
            data_samples.LISTDIR_RESPONSE_OUTPUT,
        ]
        self.repo.save_file_by_index(
            saved_index=0, filename_prefix=data_samples.OUTPUT_PREFIX
        )
        self.file_operator.save_file.assert_called_once_with(
            source_filename=data_samples.TEMP_FILENAME1,
            dest_filename=data_samples.OUTPUT_FILENAME3,
        )

    def test_save_all_files(self):
        self.file_operator.list_files.side_effect = [
            data_samples.LISTDIR_RESPONSE_TEMP,
            data_samples.LISTDIR_RESPONSE_OUTPUT,
            data_samples.LISTDIR_RESPONSE_OUTPUT_AFTER_SAVE,
        ]
        self.repo.save_all_files(filename_prefix=data_samples.OUTPUT_PREFIX)
        self.file_operator.save_file.assert_has_calls(
            [
                mock.call(
                    source_filename=data_samples.TEMP_FILENAME1,
                    dest_filename=data_samples.OUTPUT_FILENAME3,
                ),
                mock.call(
                    source_filename=data_samples.TEMP_FILENAME2,
                    dest_filename=data_samples.OUTPUT_FILENAME4,
                ),
            ]
        )

    def test_delete_file(self):
        self.file_operator.list_files.side_effect = [
            data_samples.LISTDIR_RESPONSE_TEMP,
            data_samples.LISTDIR_RESPONSE_TEMP_AFTER_DELETE,
        ]
        input_index = data_samples.LISTDIR_RESPONSE_TEMP.index(
            data_samples.TEMP_FILENAME2
        )
        result = self.repo.delete_file(input_index)
        self.file_operator.delete_file.assert_called_once_with(
            data_samples.TEMP_FILENAME2
        )
        self.assertEqual(result, input_index - 1)

    def test_delete_file_last_item(self):
        self.file_operator.list_files.side_effect = [
            data_samples.LISTDIR_RESPONSE_TEMP_AFTER_DELETE,
            [],
        ]
        input_index = data_samples.LISTDIR_RESPONSE_TEMP.index(
            data_samples.TEMP_FILENAME1
        )
        result = self.repo.delete_file(input_index)
        self.file_operator.delete_file.assert_called_once_with(
            data_samples.TEMP_FILENAME1
        )
        self.assertIsNone(result)

    def test_delete_all_files(self):
        self.file_operator.list_files.return_value = data_samples.LISTDIR_RESPONSE_TEMP
        self.repo.delete_all_files()
        self.file_operator.delete_file.assert_has_calls(
            [
                mock.call(data_samples.TEMP_FILENAME1),
                mock.call(data_samples.TEMP_FILENAME2),
            ]
        )

    def test_close(self):
        self.repo.close()
        self.file_operator.remove_dir.assert_has_calls(
            [
                mock.call(data_samples.SCANNED_IMAGES_PATH),
                mock.call(data_samples.TEMP_PATH),
            ]
        )
        self.app_deferred.callback.assert_called_once()

    def test_reset_settings(self):
        self.repo.reset_settings()
        self.config.update_config.assert_called_once()

    def test_temp_file_exists(self):
        result = self.repo.temp_file_exists
        self.file_operator.file_exists.assert_called_once_with(
            data_samples.SCANNED_TEMP_FILE_PATH
        )
        self.assertEqual(result, self.file_operator.file_exists.return_value)

    @defer.inlineCallbacks
    def test_check_snap_scanning_interfaces(self):
        coro = self.repo.check_snap_scanning_interfaces()
        result = yield defer.Deferred.fromCoroutine(coro)
        self.assertTrue(result)
