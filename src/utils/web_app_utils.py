from functools import wraps
from urllib.parse import parse_qs

from twisted.web.server import Request

from src.models import general
from src.settings import Settings
from src.utils.args_formatter import create_args_model

settings = Settings()


def decode_get_params(request: Request) -> dict:
    query_params = {}
    for key, val in request.args.items():
        query_params[key.decode("utf-8")] = (
            val[0].decode("utf-8") if isinstance(val[0], bytes) else val[0]
        )
    return query_params


def x_www_form_urlencoded(content: bytes) -> dict:
    return parse_qs(content.decode(general.Encoding.utf_8))


def decode_body(request: Request) -> dict:
    content = request.content.getvalue()
    content_loader_map = {
        general.ContentType.x_www_form_urlencoded: x_www_form_urlencoded,
    }
    content_type = request.getHeader(b"content-type").decode(general.Encoding.utf_8)
    content_loader = content_loader_map.get(content_type)
    return content_loader(content)


PARAMS_DECODER_MAP = {
    general.RESTMethod.GET: decode_get_params,
    general.RESTMethod.POST: decode_body,
    general.RESTMethod.DELETE: decode_body,
    general.RESTMethod.PUT: decode_body,
}


def format_request():
    def wrapper(fn):
        @wraps(fn)
        async def format_request_wrap(cl, request: Request, *args, **kwargs):
            method = request.method.decode()
            decode = PARAMS_DECODER_MAP.get(method)
            params_dict = decode(request)

            alerts = []
            fn_params = [cl, request, alerts]
            if params_dict:
                model = create_args_model(
                    path=request.path.decode(), params_dict=params_dict
                )
                fn_params.append(model)
            return await fn(*fn_params, *args, **kwargs)

        return format_request_wrap

    return wrapper
