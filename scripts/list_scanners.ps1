$wiaManager = New-Object -ComObject WIA.DeviceManager

$devices = $wiaManager.DeviceInfos

$scanners = @()

foreach ($device in $devices) {
    $scanners += $device.Properties("Name").Value
}

if ($scanners.Count -eq 0) {
    return ""
} else {
    $scanners | ForEach-Object { Write-Host $_ }
}

return $scanners
