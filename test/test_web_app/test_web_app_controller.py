from unittest import mock

from twisted.trial import unittest

from src.web_app.web_app_controller import WebAppController
from test.patchers.settings import patch_settings

MODULE_PATH = "src.web_app.web_app_controller"


class TestWebAppController(unittest.TestCase):
    def setUp(self):
        patch_settings([MODULE_PATH])
        self.app = mock.MagicMock()
        self.repo = mock.MagicMock()
        self.patch_general_routes()
        self.web_app_controller = WebAppController(
            app=self.app,
            app_deferred=mock.MagicMock(),
        )

    def patch_general_routes(self):
        self.general_routes = mock.MagicMock()
        patcher = mock.patch(MODULE_PATH + ".GeneralRoutes", self.general_routes)
        patcher.start()

    def test_start(self):
        self.web_app_controller.start(self.repo)
        self.general_routes.assert_called_once_with(repo=self.repo)
        self.app.route.assert_called()
