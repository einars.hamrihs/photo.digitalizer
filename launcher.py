import sys

from twisted.scripts.twistd import run

from src.utils.init_utils import create_temp_dir
from src.utils.path_utils import get_path
from src.utils.sys_utils import is_windows


def launcher():
    temp_dir = create_temp_dir()
    sys.argv.extend(["-ny", get_path("service.tac")])
    if not is_windows():
        sys.argv.append(f"--pidfile={temp_dir}pixelyse.pid")
    sys.exit(run())


if __name__ == "__main__":
    launcher()
