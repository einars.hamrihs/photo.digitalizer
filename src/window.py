import os

from PyQt6.QtCore import QUrl, Qt
from PyQt6.QtGui import QIcon
from PyQt6.QtWebEngineWidgets import QWebEngineView
from PyQt6.QtWidgets import QMainWindow

from src.repository import Repository
from src.settings import Settings

settings = Settings()
# QtWebengine throws a warning if there's no directory for spellcheck directories
os.environ["QTWEBENGINE_DICTIONARIES_PATH"] = settings.web_dictionaries_location


class MainWindow(QMainWindow):
    def __init__(self, repo: Repository, *args, **kwargs):
        super().__init__()
        self.repo = repo
        self.setWindowTitle(settings.app_name)
        self.setWindowIcon(QIcon(settings.icon_location))
        self.setMinimumSize(settings.min_window_width, settings.min_window_height)
        self.resize(settings.default_window_width, settings.default_window_height)

        self.browser = QWebEngineView()
        self.browser.setUrl(QUrl(f"http://127.0.0.1:{settings.web_port}"))
        self.browser.setContextMenuPolicy(Qt.ContextMenuPolicy.NoContextMenu)
        self.setCentralWidget(self.browser)

    def closeEvent(self, event):
        self.repo.close()
        event.accept()
