#!/bin/bash
source app_details.sh

# Create the control file
cat <<EOF > $PACKAGE_DIR/DEBIAN/control
Package: $PACKAGE_NAME
Version: $VERSION
Maintainer: $MAINTAINER
Architecture: $ARCHITECTURE
Description: $SUMMARY
 $DESCRIPTION
Homepage: $HOMEPAGE
Provides: $PACKAGE_NAME
Conflicts: $PACKAGE_NAME
Replaces: $PACKAGE_NAME
Depends: simple-scan
EOF
